-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2014 at 12:37 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jogja-gowes`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menus`
--

CREATE TABLE IF NOT EXISTS `admin_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(10) NOT NULL,
  `icon` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `admin_menus`
--

INSERT INTO `admin_menus` (`id`, `menu`, `icon`) VALUES
(3, 'Content', 'icon-list-alt'),
(4, 'Settings', 'icon-wrench');

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=716 ;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(715, 12, '2.2.2.2', '12'),
(714, 12, '2.2.2.2', '12'),
(713, 12, '2.2.2.2', '12'),
(712, 12, '2.2.2.2', '12');

-- --------------------------------------------------------

--
-- Table structure for table `ci_query`
--

CREATE TABLE IF NOT EXISTS `ci_query` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `query_string` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=906 ;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('d901185b8f9ef71088ec68193001b937', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36', 1411167080, 'a:5:{s:9:"user_data";s:0:"";s:8:"username";s:12:"ari pratomo1";s:5:"email";s:15:"admin@admin.com";s:7:"user_id";s:2:"42";s:14:"old_last_login";s:10:"1410488735";}'),
('a744190576e8783a836b02c715fc78f8', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36', 1411252616, '');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `judul` varchar(35) NOT NULL,
  `konten` text NOT NULL,
  `picture` varchar(50) NOT NULL,
  `tanggal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `judul`, `konten`, `picture`, `tanggal`) VALUES
(2, 'Gowes Goes To School 2', 'Musisi kondang Ahmad Dhani mengaku ditipu oleh agen artis bernama Tommy Pratama dengan kerugian senilai US$ 860.000 (setara Rp 9,8 miliar) sehingga dia melaporkan kasus tersebut ke Polda Metro Jaya.\r\n\r\nDhani mengadukan kasus penipuan tersebut, Rabu (10/9), dan menunjukkan sejumlah bukti atas keterlibatan Tommy yang dinilai gagal mendatangkan grup musik ternama kelas dunia Metallica untuk manggung di Jakarta tahun 2011 lalu.\r\n\r\n“Kami terpaksa harus melaporkan kasus penipuan tersebut. Karena agen Tommy Pratama semula berjanji bisa mendatangkan grup musik Metallica ke Indonesia namun ternyata tidak terlaksana. Tommy juga belum mengembalikan uang senilai US$ 860.000 atau sekitar Rp 9,8 miliar yang diberikan tahun 2011 untuk membayar Metallica," ujar Ahmad Dhani, di Polda Metro Jaya, Jakarta, Rabu (10/9).\r\n\r\nMenurut Dani, upaya menanyakan perkembangan terkait batalnya Metallica melalui kuasa hukum sudah dilakukan lewat mediasi tentang masalah ini.', '91358f13c140e49ac7af9c4716ce0bb4.jpg', '12-09-2014'),
(3, 'GathNas Ngepit IV', 'James Franco is many things: film star, director, soap opera alum, former Oscar host, poet, Broadway lead, film school graduate, heartthrob, teacher. But please, Internet, he''s not the selfie king.\r\n\r\nDuring a small press gathering this week to promote his new AOL On Originals series called Making a Scene — a humorous collection of digital shorts that mashes up two iconic films in one (like Dirty Dancing and Reservoir Dogs), Franco said he''s undeserving of the title of the web''s premiere selfie expert.', '38cf59cfa3cc5102fd0a51ac72500b9f.jpg', '20-09-2014');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'General User'),
(3, 'root', 'Root is super user'),
(4, 'customer', 'General Member Customer'),
(5, 'unit', 'Unit user');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `on_menu` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `in_admin` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`uri`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=64 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `uri`, `on_menu`, `in_admin`, `parent_id`, `order`) VALUES
(39, 'Site Config', 'gowespanel/settings', '4', 1, 0, 0),
(40, 'Users', 'gowespanel/users', '4', 1, 0, 0),
(62, 'Promo', 'gowespanel/promo', '3', 1, 0, 0),
(60, 'Notifikasi', 'gowespanel/notifikasi', '3', 1, 0, 0),
(61, 'News', 'gowespanel/news', '3', 1, 0, 0),
(63, 'Event', 'gowespanel/event', '3', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `konten` text NOT NULL,
  `tanggal` varchar(15) NOT NULL,
  `picture` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `judul`, `konten`, `tanggal`, `picture`) VALUES
(1, 'Jambore Goweser', 'It frees the memory associated with the result and deletes the result resource ID. Normally PHP frees its memory automatically at the end of script execution. However, if you are running a lot of queries in a particular script you might want to free the result after each query result has been generated in order to cut down on memory consumptions. Example:\r\n\r\nThe number of FIELDS (columns) returned by the query. Make sure to call the function using your query result object:', '20 / 08 / 2014', ''),
(2, 'Kunjungan Bupati', 'Displays the number of affected rows, when doing "write" type queries (insert, update, etc.).\r\n\r\nNote: In MySQL "DELETE FROM TABLE" returns 0 affected rows. The database class has a small hack that allows it to return the correct number of affected rows. By default this hack is enabled but it can be turned off in the database driver file.', '28 / 08 / 2014', ''),
(3, 'Pit-Pitan 17an', 'CodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. \r\n\r\nCodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.', '29 / 08 / 2014', 'f70678a1fae6e331ed852749fdbae5a0.jpg'),
(5, 'Ice Bucket Challenge with Liquid Nitrogen?!', 'The Ice bucket challenge for ALS has been a global phenomenon, with all sorts of people from celebrities and businessmen to your average Joe taking part.  Some attempts brought epic fails to the table. If there was an award for the craziest IBC attempt, then the award should go to chemist Muhammad Qureshi.  He decided to take the game to the next level.  The chemist used liquid nitrogen instead of water for the challenge.\r\n\r\nFor those of you who are reading this and are not scientifically minded, I will try and break it down for you.  If you don’t know, nitrogen has to be extremely cold before it becomes a liquid.  In fact, it has a boiling point of ?321 °F and it has a freezing point of ?346 °F.\r\n\r\nQureshi trusts the laws of Chemistry and it’s a good job he knew what he was doing.  When he undertook the challenge, he did not get immediate frostbite after pouring the liquid nitrogen on himself and manages to survive the incident without a scratch, or burn come to think of it.  He did emphasize, however, that NO ONE should try this at home. Or anywhere else for that matter!\r\n\r\nhttps://www.youtube.com/watch?v=Xj-prpHfyEY\r\n\r\nSo, why did Qureshi not suffer immediate frost burn? The reason is because of something called theLeidenfrost Effect, which occurs when a surface temp is so hot that it creates a thin layer of vapor, which then lies between the surface itself and the liquid.  This causes the liquid to become insulated and slows down evaporation.  Clear?\r\n\r\nIn layman’s terms, Qureshi’s body temperature was so hot in relation to the temperature of the liquid nitrogen, that contact between the two surfaces creates an insulating barrier that saved him from suffering any immediate harm.\r\n\r\nThis does not mean, however that you can sit around after pouring liquid Nitrogen all over yourself.  If you notice in the video, Qureshi had to constantly  shake off any liquid from his hair, his skin and his clothes immediately after pouring it onto himself to make sure none of it got stuck anywhere.', '06 / 09 / 2014', 'd7bc0101cab653b9fb7c782e07283cc2.jpg'),
(6, 'Pemilihan Ketua Fixie', 'So, why did Qureshi not suffer immediate frost burn? The reason is because of something called theLeidenfrost Effect, which occurs when a surface temp is so hot that it creates a thin layer of vapor, which then lies between the surface itself and the liquid. This causes the liquid to become insulated and slows down evaporation. Clear?\r\n\r\nIn layman’s terms, Qureshi’s body temperature was so hot in relation to the temperature of the liquid nitrogen, that contact between the two surfaces creates an insulating barrier that saved him from suffering any immediate harm.\r\n\r\nThis does not mean, however that you can sit around after pouring liquid Nitrogen all over yourself. If you notice in the video, Qureshi had to constantly shake off any liquid from his hair, his skin and his clothes immediately after pouring it onto himself to make sure none of it got stuck anywhere.', '06 / 09 / 2014', 'cebfc531ce1c0f62d807460a999fabfe.jpg'),
(7, 'tes', 'setet', '06 / 09 / 2014', '61466247ac361902b54a0371af3e5192.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `notifikasi`
--

CREATE TABLE IF NOT EXISTS `notifikasi` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `judul` varchar(25) NOT NULL,
  `konten` text NOT NULL,
  `publish` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `notifikasi`
--

INSERT INTO `notifikasi` (`id`, `judul`, `konten`, `publish`) VALUES
(1, 'Event Terdekat', 'Hampir selesai saudara2', '03 / 09 / 2014');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(100) DEFAULT NULL,
  `unit_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `text` text,
  `meta_kw` text,
  `meta_desc` text,
  `show_on` enum('top','navigation','both') DEFAULT NULL,
  `status` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `link_to` text NOT NULL,
  `new_window` int(11) NOT NULL,
  `menu` text NOT NULL,
  `heading` text NOT NULL,
  `background` varchar(100) NOT NULL,
  `set_order` int(11) NOT NULL,
  `video_url` varchar(100) NOT NULL,
  `album_id` int(11) NOT NULL,
  `welcome_page` int(11) NOT NULL,
  `attachment` varchar(100) NOT NULL,
  `welcome_text` int(11) NOT NULL,
  `longitude` varchar(111) NOT NULL,
  `latitude` varchar(111) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=172 ;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains a list of modules that a group can access.' AUTO_INCREMENT=110 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `user_id`, `module_id`) VALUES
(108, 57, 0),
(109, 59, 61);

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--

CREATE TABLE IF NOT EXISTS `promo` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `judul` varchar(30) NOT NULL,
  `konten` text NOT NULL,
  `tanggal` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `promo`
--

INSERT INTO `promo` (`id`, `judul`, `konten`, `tanggal`) VALUES
(1, 'Gebyar Indomaret', 'You want a framework with a small footprint.\r\nYou need exceptional performance.\r\nYou need broad compatibility with standard hosting accounts that run a variety of PHP versions and configurations.\r\nYou want a framework that requires nearly zero configuration.\r\nYou want a framework that does not require you to use the command line.\r\nYou want a framework that does not require you to adhere to restrictive coding rules.\r\nYou do not want to be forced to learn a templating language (although a template parser is optionally available if you desire one).\r\nYou eschew complexity, favoring simple solutions.\r\nYou need clear, thorough documentation.', '02 / 09 / 2014'),
(2, 'Sari Husada Sepeda Gembira 3', 'CodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries.', '02 / 09 / 2014');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(123) NOT NULL,
  `title` varchar(123) NOT NULL,
  `description` text NOT NULL,
  `type` text NOT NULL,
  `value` text NOT NULL,
  `options` text NOT NULL,
  `module` varchar(123) NOT NULL,
  `is_gui` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `title`, `description`, `type`, `value`, `options`, `module`, `is_gui`) VALUES
(1, 'admin_theme', 'Admin Theme', '', '', 'admin', '', 'theme', 0),
(2, 'frontend_enabled', 'Site Status', 'Use this option to the user-facing part of the site on or off. Useful when you want to take the site down for maintenence', 'radio', '0', '1=Open|0=Closed', 'general', 1),
(3, 'default_theme', 'Theme', 'Select the theme you want users to see by default.	', '', 'echo', '', 'theme', 0),
(4, 'address', 'Address', '', 'textarea', 'Address', '', 'contact', 1),
(5, 'facebook', 'Facebook', 'Facebook username', 'text', 'jogja-gowes', '', 'social', 1),
(6, 'twitter', 'Twitter', 'Twitter username.', 'text', 'jogja-gowes', '', 'social', 1),
(7, 'mobile', 'Mobile', 'Mobile phone number', 'text', '11111', '', 'contact', 1),
(8, 'phone', 'Phone', 'Phone number', 'text', '0', '', 'contact', 1),
(9, 'email', 'Email', 'All e-mails from users, guests and the site will go to this e-mail address.', 'text', '0', '', 'contact', 1),
(10, 'site_name', 'Site Name', 'The name of the website for page titles and for use around the site.', 'text', 'Jogja-gowes', '', 'general', 1),
(11, 'meta_topic', 'Meta Topic', 'Two or three words describing this type of company/website.', 'text', 'Jogja-gowes', '', 'generalXX', 0),
(12, 'site_slogan', 'Site Slogan', 'The slogan of the website for page titles and for use around the site.', 'text', 'Jogja-gowes', '', 'general', 1),
(13, 'meta_desc', 'Meta Description', 'Two or three words to define a description of a website/company', 'text', 'Portal Komunitas Jogja Gowes', '', 'general', 1),
(14, 'meta_keyword', 'Meta Keyword', 'meta keyword defines keywords for a website/company', 'text', 'jeee, lorem, ipsum', '', 'general', 1),
(15, 'currency', 'Currency ', 'Set Default Currency ', 'radio', 'Rp', '1=Rp|2=USD', 'general', 1),
(17, 'layout', 'Layout Themes', 'Layout Animation', 'radio', '0', '', 'theme', 1),
(20, 'longitude', 'Longitude', '', 'text', '105.546341', '', 'contact', 1),
(21, 'latitude', 'Latitude', '', 'text', '-5.664502', '', 'contact', 1),
(27, 'bg_image', 'Background Image', 'Background Image', 'file', '25aaf36719342839f1e2acfec38b2d2d.jpg', '', 'theme', 1),
(28, 'bg_flash', 'Falsh Background', 'Falsh Background', 'file', 'c1556ec296c447535d07b388ecaed7d5.swf', '', 'theme', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` int(10) unsigned NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `is_root` int(11) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `zip` varchar(8) NOT NULL,
  `phone` varchar(14) NOT NULL,
  `subscribe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `is_root`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `city_id`, `province_id`, `address`, `zip`, `phone`, `subscribe`) VALUES
(42, 2130706433, 'ari pratomo1', '8b4d4500999a029344c310013f9b6bfeee069d91', 0, NULL, 'admin@admin.com', NULL, NULL, '8de6260b24af32ff1d3acf82b2bae06ce1af3009', 1337618795, 1411159107, 1, 'admin', 'istrator', 1, 1, '', '', '', 0),
(59, 0, 'jan cuker', 'b475cec477fe3eac0f96a67aaa768d9dd67b0fcb', 0, NULL, 'jan@cuker.com', NULL, NULL, NULL, 1409211134, 1409211158, 1, 'Jan', 'Cuker', 0, 0, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(73, 49, 4),
(65, 42, 1),
(67, 44, 4),
(71, 48, 1),
(72, 42, 3),
(80, 56, 1),
(83, 59, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
