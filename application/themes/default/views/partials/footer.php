<!-- footer area -->    
<footer>
	<div id="colophon" class="wrapper clearfix">
    	<div class="grid_3" ><?php echo $this->calendar->generate() ;?></div>
    	<div class="grid_3" >footer stuff</div>
    	<div class="grid_3" >footer stuff</div>
    	<div id="attribution" class="wrapper clearfix grid_3" style="color:#666; font-size:11px;">
    		Site built with : 
    		<br/><a href="http://www.prowebdesign.ro/simple-responsive-template/" target="_blank" title="Simple Responsive Template is a free software by www.prowebdesign.ro" style="color:#777;">Simple Responsive Template</a>
    		<br/><a href="https://ellislab.com/codeigniter" target="_blank" title="Codeigniter" style="color:#777;">Codeigniter Framework</a>
    		<br/><a href="http://getbootstrap.com/" target="_blank" title="Bootstrap CSS Framework" style="color:#777;">Bootstrap 2.3</a>
    		<br/><a href="http://jqueryui.com/" target="_blank" title="jQuery UI" style="color:#777;">jQuery UI</a>
    		<br/>and other reusable stuff
    	</div>
    </div>

    <!--You can NOT remove this attribution statement from any page, unless you get the permission from prowebdesign.ro-->    
</footer><!-- #end footer area --> 


<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.9.0.min.js">\x3C/script>')</script>

<?php echo theme_js('flexslider/jquery.flexslider-min.js', 'defer'); ?>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php echo theme_js('bootstrap.min.js'); ?>

<!-- fire ups - read this file!  -->   
<script src="<?php echo site_url('assets') ;?>/js/main.js"></script>

