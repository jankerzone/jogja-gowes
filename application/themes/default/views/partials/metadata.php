<base href="<?php echo BASE_URL?>" />

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="description" content="Portal Komunitas Sepeda yang menyatukan para pegowes di seantero Jogja">
<meta name="keywords" content="">

<!-- Mobile viewport -->
<meta name="viewport" content="width=device-width; initial-scale=1.0">

<link rel="shortcut icon" href="<?php echo site_url('assets') ;?>/images/favicon.ico"  type="image/x-icon" />

<!-- CSS-->
<!-- Google web fonts. You can get your own bundle at http://www.google.com/fonts. Don't forget to update the CSS accordingly!-->
<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic|Oswald:700' rel='stylesheet' type='text/css'>
<link href="<?php echo site_url()?>application/themes/default/js/flexslider/flexslider.css" type="text/css" rel="stylesheet" />

<?php echo theme_css('normalize.css');?>
<?php echo theme_css('colorblocks-style.css');?>
<?php //echo theme_css('bootstrap.min.css');?>
<!-- end CSS-->
    
<!-- JS-->
<?php echo theme_js('libs/modernizr-2.6.2.min.js');?>
<?php echo theme_js('bootstrap.min.js');?>
<!-- end JS-->
