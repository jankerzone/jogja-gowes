<body id="home">
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

<!-- header area -->
    <header class="wrapper clearfix">
		       
        <div id="banner">        
        	<div id="logo"><a href="<?php echo BASE_URL() ;?>"><img src="<?php echo site_url('assets') ;?>/images/logo-gowes3.svg" alt="logo" width="151px"></a></div> 
        </div>
        
        <!-- main navigation -->
        <nav id="topnav" role="navigation">
        <div class="menu-toggle">Menu</div>  
        	<ul class="srt-menu" id="menu-main-navigation">
            <li><a href="<?php echo base_url() ;?>">Home page</a></li>
			<li><a href="#">Informasi</a>
				<ul>
					<li><a href="<?php echo site_url('news') ;?>">Berita</a></li>
					<li><a href="<?php echo site_url('promo') ;?>">Promo Acara</a></li>
					<li><a href="<?php echo site_url('profil') ;?>">Profil</a></li>
				</ul>
			</li>
			<li><a href="#">Fitur</a>
				<ul>
					<li><a href="<?php echo site_url('diskusi') ;?>">Forum Diskusi</a></li>
					<li><a href="#">Jual/Beli</a></li>
					<li class="current"><a href="<?php echo site_url('jalurgowes')?>">Sistem Pencarian Jalur Gowes</a></li>
				</ul>
			</li>
			<li class="current" >
      <?php   if ($this->ion_auth->logged_in()) { ?>
      <a href="<?php echo site_url()?>users/logout" data-target=".bs-example-modal-sm">Logout</a>
    <?php } else { ?>
				<a href="<?php echo site_url()?>users" data-target=".bs-example-modal-sm">Login Area</a>
		<?php } ?>	
      </li>	
		</ul>     
		</nav><!-- #topnav -->
  
    </header><!-- end header -->
 
 <?php $a = array("page-header","page-header2","page-header3"); 
 shuffle($a); ?>
<!-- hero area (the grey one with a slider -->
    <section id="<?php echo $a[0];?>" class="clearfix">    
   
    <div class="wrapper">
      <h1><?php echo $tagline ?></h1>
    </div>
    </section><!-- end hero area -->


<!-- Penampakan Modal 
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="container" >
      <div class="col-xs-3">
      <?php //echo form_open("users",'class="form-signin" role="form"');?>
        <h2 class="form-signin-heading">Dashboard</h2>
        <input type="text" class="form-control" placeholder="Email address" name="identity" value="" id="identity" required autofocus>
        <input type="password" class="form-control" placeholder="Password" name="password" value="" id="password" required>
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Ingat saya
        </label>
        <button class="btn btn-lg btn-success btn-block" type="submit">Login</button></br>
      </form>
      </div>
    </div> 
    </div>
  </div>
</div>
-->
<!-- main content area -->   
   
<div id="main" class="wrapper clearfix">  