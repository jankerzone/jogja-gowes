<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Default extends Theme {

    public $name = '';
    public $author = '';
    public $author_website = '';
    public $website = '';
    public $description = '';
    public $version = '1.0';

}

/* End of file theme.php */