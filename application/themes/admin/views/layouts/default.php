<!DOCTYPE html>
<html>
	<head>
            <?php echo $template['partials']['metadata']; ?>
            <title><?php echo $template['title']; ?></title>    
	</head>
        <body style="">
            <?php echo $template['partials']['navigation']; ?>               
            <div class="container well" style="min-height: 500px">
                    <legend><?php echo $template['title']; ?></legend>
                     <?php if(isset ($template['partials']['nav-pills'])){ echo $template['partials']['nav-pills'];}?>
                    <?php echo $this->session->flashdata('response');?>
                <?php echo $template['body']; ?>
                </div>
            
		<div id="ajaxpop"></div>

                
                <?php echo $template['partials']['footer']; ?>
	</body>
        
</html>