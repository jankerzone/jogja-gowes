<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
<div class="container" style="width: 94%;">
<a class="brand" href="#">
JOGJA-GOWES
</a>
   <?php $uri = $this->uri->segment(2);?>
<ul class="nav">
    <li><?php echo anchor(PANEL_URL,'<i class="icon-home icon-white"></i>Dashboard')?></li>
    
 

    
   <?php $group =array('root'); if ($this->ion_auth->in_group($group)){  $i=1; foreach ($rootmenu_items as $menu_item){?>
 <li class="dropdown" id="menu<?php echo $i?>">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#menu<?php echo $i?>">
     <i class="<?php echo $menu_item->icon?> icon-white"></i> <?php echo $menu_item->menu?>
      <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
        <?php $modules = $this->module_model->get_many_by(array('on_menu'=>$menu_item->id,'in_admin'=>1,'parent_id'=>0),array($this->db->order_by('order','asc')));
        foreach($modules as $module){?>
      <li><?php echo anchor($module->uri,$module->name)?></li>
      <?php }?>
    </ul>
  </li>    
<?php $i++; } }else {?>

   <?php  $i=1; foreach ($usermenu_items as $menu_item){?>
 <li class="dropdown" id="menu<?php echo $i?>">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#menu<?php echo $i?>">
     <i class="<?php echo $menu_item->icon?> icon-white"></i> <?php echo $menu_item->menu?>
      <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
        <?php $modules = $this->permission_model->get_module($menu_item->menu_id);
        foreach($modules as $module){?>
      <li><?php echo anchor($module->uri,$module->name)?></li>
      <?php }?>
    </ul>
      
  </li>

  <?php $i++; } }?>

</ul>
  
  <ul class="nav pull-right">
      
      <li  class="dropdown " id="menu5" ><a  class="dropdown-toggle" data-toggle="dropdown" href="menu4#"><i class="icon-th-large icon-white"></i>My Profile<b class="caret"></b></a>
          <ul class="dropdown-menu">
               <li><a href="<?php echo site_url('gowespanel/users/change_mypassword')?>"><i class="icon-lock"></i>Change Password</a></li>
              <li class="divider"></li>
              <li><a href="<?php echo site_url('users/logout')?>"><i class="icon-off"></i>Logout</a></li>
          </ul>
      </li>
  </ul>    
      
</div>                                    			
</div>
</div>