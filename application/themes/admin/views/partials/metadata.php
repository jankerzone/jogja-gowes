<base href="<?php echo BASE_URL?>" />
<?php echo theme_css('style.css');
echo theme_css('bootstrap.css');
echo theme_css('jqueryui/jquery-ui.css');
echo theme_js('jquery/jquery.min.js');
echo theme_js('ajax.js');
echo theme_js('ajaxfileupload.js');
echo theme_js('jquery/jqueryui.js');
echo theme_js('jquery/jquery.cookie.js');
echo theme_js('bootstrap.min.js');
echo $template['metadata'];
?>
