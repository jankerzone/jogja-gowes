(function($){

	$.doQuery = function (queryString, params) 
	{		
		$.extend(params, {'ci_csrf_token': $.cookie('ci_csrf_token')});
		
		var request = $.ajax({
			type: "POST",
			url: queryString,
			data: params,
			dataType: "html"
		});
		
		request.done(function( data ) {
			eval(data);
		});
		
		request.fail(function(jqXHR, textStatus) {
			  console.log( "Request failed: " + textStatus );
		});
	}
        
        
	
	$.openPop = function (popupUrl, params) 
	{
		$.extend(params, {'ci_csrf_token': $.cookie('ci_csrf_token')});
		
		$('#ajaxpop').load(popupUrl, params);
	}
        
	$.ajaxUpload = function(queryUrl, params) 
	{
		$.extend(params, {'ci_csrf_token': $.cookie('ci_csrf_token')});
		
		$.ajaxFileUpload({
			url: queryUrl,
			secureuri: false,
			fileElementId: 'userfile',
			dataType: 'json',
			data: params,
			success: function (data, status)
			{
				if(data.status != 'error')
				{
					$('.response').html('<p>Background Successfully to upload</p>');

				}
				else
				{
					$('.response').html(data.msg);
				}
			}
		
		});
		
		return false;
	};        
        
})(jQuery);