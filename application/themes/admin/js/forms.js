function CheckAll(formobj) 
{
	for (i=0; i < formobj.length; i++) 
		if (formobj.elements[i].name.substr(0,8) == 'checked_') 
			formobj.elements[i].checked = formobj.checkall.checked;
}

function ConfirmDelete(form, msg) {
	var godel = window.confirm(msg);
	if (godel) {
		form.elements['suredelete'].value = 1;
		form.submit();
	}
}

function showTo(value)
{
	$('#url_type').hide();
	$('#page_type').hide();
	$('#module_type').hide();
	$('#'+value).show();
}