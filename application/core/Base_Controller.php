<?php

class Base_Controller extends MX_Controller{

	public function __construct()
	{

                $this->asset->set_theme(APPPATH.'themes/default');      
                $this->config->set_item('theme_asset_dir', dirname('themes').'/');
				$this->config->set_item('theme_asset_url', BASE_URL.dirname('themes').'/');
                $this->template->set_theme('default')
                     ->enable_parser(FALSE)
                     ->set_layout('default')
                     ->set_partial('metadata', 'partials/metadata')
                     ->set_partial('header', 'partials/header')
                     ->set_partial('sidebar','partials/sidebar')
                     ->set_partial('footer', 'partials/footer');  

                //tambah cache biar ngebut..
                //$this->output->cache(15);
                     //comot library ion auth
                     $this->load->library('users/ion_auth');

	}

	public function testt(){
		return 'Hello';
	}

}

?>