<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."third_party/MX/Controller.php";

// Code here is run before ALL controllers
class MY_Controller extends MX_Controller {

	// Deprecated: No longer used globally
	protected $data;
	
	public $module;
	public $controller;
	public $method;
    public $current_user;
	public function __construct()
	{
		parent::__construct();
                
		$this->benchmark->mark('my_controller_start');

                $this->load->library('users/ion_auth');
                
                $this->load->library('settings/settings');
                
		// Work out module, controller and method and make them accessable throught the CI instance
		ci()->module = $this->module = $this->router->fetch_module();
		ci()->controller = $this->controller = $this->router->fetch_class();
		ci()->method = $this->method = $this->router->fetch_method();

		// Loaded after $this->current_user is set so that data can be used everywhere
		$this->load->model(array(
                        'pages/page_model',
                        'menu/menu_model',
                        'users/permission_model',
                        'menu/admin_menu_model',
						'modules/module_model',
						'themes/theme_model',
                        'settings/setting_model',
		));                
               
                
                
		$this->benchmark->mark('my_controller_end');
		
	
	}
}

/**
 * Returns the CI object.
 *
 * Example: ci()->db->get('table');
 *
 * @staticvar	object	$ci
 * @return		object
 */
function ci()
{
	return get_instance();
}
