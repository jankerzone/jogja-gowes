<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Admin_Controller extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
         //self::_check_access();
        // Prepare Asset library
        
        if (!$this->ion_auth->logged_in())
        {
                redirect('users');
        }       
        elseif (!$this->ion_auth->is_admin() && !$this->ion_auth->is_unit())
        {
        //redirect them to the home page because they must be an administrator to view this
        redirect('users', 'refresh');
        }
        
        $this->asset->set_theme(APPPATH.'themes/'.$this->settings->admin_theme.'');

        // Set the location of assets
        $this->config->set_item('asset_dir', dirname('themes').'/');
        $this->config->set_item('asset_url', BASE_URL.dirname('themes').'/');
        $this->config->set_item('theme_asset_dir', dirname('themes').'/');
        $this->config->set_item('theme_asset_url', BASE_URL.dirname('themes').'/');
        
        // Template configuration
        $this->template
                ->enable_parser(FALSE)
                ->set_theme(''.$this->settings->admin_theme.'')
                ->set_partial('navigation','partials/navigation')
                ->set_partial('metadata','partials/metadata')
                ->set_partial('footer', 'partials/footer')
                ->set_layout('default');
       $this->rootmenu();
       $this->usermenu();
        $group =array('root'); 
        if(!$this->ion_auth->in_group(array('root')))
        if(($this->uri->segment(2))){
            if($this->uri->segment(3) != 'change_mypassword' && $this->uri->segment(3) != 'unit' && $this->uri->segment(4) != 'unit' && $this->uri->segment(5) != 'unit')
            if($this->check_permission()==FALSE){
                $this->session->set_flashdata('response', show_msg("You don't have permission"));
                redirect(PANEL_URL);
            }
            
        }
            
        }
        

        
       private function rootmenu(){
           $modules = array();
           $menus = $this->admin_menu_model->get_all();
       
           $this->template->rootmenu_items = $menus;
           $this->template->modules = $modules;
       }
      
       private function usermenu(){
           $menus = $this->permission_model->get_menu_item();
           $this->template->usermenu_items = $menus;
       }
       
       private function check_permission(){
           //http://localhost/interaksi_project/eco/ecopanel/backgrounds/flash
           if($this->uri->segment(2)=='backgrounds' && $this->uri->segment(3)=='flash' || $this->uri->segment(3)=='image'){
           $uri =     $this->uri->segment(2).'/'. $this->uri->segment(3).'/';
           }else{
           $uri = $this->uri->segment(2);
           }
           
           $cek = $this->permission_model->cek_permission(PANEL_URL.'/'.$uri.'');
           
           if(!empty ($cek)){
               return TRUE;
           }else{
               return FALSE;
           }
           
           
           
       }
        /*
	private function _check_access()
	{
		// These pages get past permission checks
		$ignored_pages = array('admin/login', 'admin/logout', 'admin/help');

		// Check if the current page is to be ignored
		$current_page = $this->uri->segment(1, '') . '/' . $this->uri->segment(2, 'index');

		// Dont need to log in, this is an open page
		if (in_array($current_page, $ignored_pages))
		{
			return TRUE;
		}
		else if ( ! $this->current_user)
		{
			redirect('admin/login');
		}

		// Admins can go straight in
		else if ($this->current_user->group === 'admin')
		{
			return TRUE;
		}

		// Well they at least better have permissions!
		else if ($this->current_user)
		{
			// We are looking at the index page. Show it if they have ANY admin access at all
			if ($current_page == 'admin/index' && $this->permissions)
			{
				return TRUE;
			}
			else
			{
				// Check if the current user can view that page
				return array_key_exists($this->module, $this->permissions);
			}
		}

		// god knows what this is... erm...
		return FALSE;
	}        
    */
}

?>
