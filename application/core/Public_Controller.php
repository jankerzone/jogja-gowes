<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Code here is run before frontend controllers
class Public_Controller extends MY_Controller
{
        public $theme;
        protected $frontend_enabled;
        public function __construct()
	{
		parent::__construct();
                $this->benchmark->mark('public_controller_start');

		
                if ($this->settings->frontend_enabled == 0)
		{
			header('Retry-After: 600');
			
			$error = '<h1>Site under construction</h1>';
			show_error($error, $status_code=500, $heading="Opps");
		}
                
                
             
                $this->load->library('cart');
                $this->asset->set_theme(APPPATH.'themes/'.$this->settings->default_theme);      
                $this->config->set_item('theme_asset_dir', dirname('themes').'/');
		$this->config->set_item('theme_asset_url', BASE_URL.dirname('themes').'/');
                $this->template->set_theme($this->settings->default_theme)
                     ->enable_parser(FALSE)
                     ->set_layout($this->settings->layout)
                     ->set_partial('metadata', 'partials/metadata');       
                 
		$this->benchmark->mark('public_controller_end');
	}
        
    
}
