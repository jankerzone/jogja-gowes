<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Code here is run before frontend controllers
class Auth_Controller extends MY_Controller
{
        public $theme;
        
	public function __construct()
	{
		parent::__construct();
                $this->benchmark->mark('my_controller_start');
                $this->asset->set_theme(APPPATH.'themes/'.$this->theme='auth');      
                $this->config->set_item('theme_asset_dir', dirname('themes').'/');
		$this->config->set_item('theme_asset_url', BASE_URL.dirname('themes').'/');

                $this->template->set_theme('auth')
                     ->set_layout('default')
                     ->set_partial('metadata', 'partials/metadata')
                     ->inject_partial('footer', '<h6></h6>')
                     ->append_metadata( '
				<script type="text/javascript">
					var APPPATH_URI = "'.APPPATH_URI.'";
					var BASE_URI = "'.BASE_URI.'";
				</script>' );
		$this->benchmark->mark('my_controller_end');
	}
}
