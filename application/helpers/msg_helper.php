<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	function show_msg($msg, $class=FALSE, $width=NULL) 
	{
		if(is_array($msg))
		{
			$mesage = isset($msg['mesage'])?$msg['mesage']:'';
			$class = isset($msg['class'])?$msg['class']:'info';
			$width = isset($msg['width'])?'width: ' . $msg['width']:'width: 95%;';
			if(!preg_match('/%;/', $width)) $width = $width . 'px;';
		}
		else 
		{
			if ($width) 
			{ 
				$width = 'width: '.$width;	
			}
			
			if(!$class)
			{
				$class = 'info';				
			}
			
			$class = 'alert-' . $class;
			
			$mesage = $msg;
		}
		
		return $mesage?'<div class="alert ' . $class . '" style="' . $width . '">' . $mesage . '</div>':'';
	}

