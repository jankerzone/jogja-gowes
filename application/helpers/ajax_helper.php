<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function AjaxMessage($title, $message)
{
	$html = '<div id="ajaxmessage" title="'.$title.'">'.$message.'</div>';
	
	return AjaxContent($html);
}

function AjaxContent($content)
{
	$content = str_replace("\n", '', $content);
	$content = str_replace("\r", '', $content);
	$content = addslashes(trim($content));
	
	return $content;
}

