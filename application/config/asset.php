<?php defined('BASEPATH') OR exit('No direct script access allowed');


$config['asset_img_dir'] = 'img';
$config['asset_js_dir'] = 'js';
$config['asset_css_dir'] = 'css';
$config['asset_swf_dir'] = 'swf';