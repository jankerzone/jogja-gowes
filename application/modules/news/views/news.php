<h3>Berita terkait seputar Jogja-Gowes</h3>
<!-- content area -->    
	<section id="content">
    	<div class="clearfix">
	    	<?php foreach($news as $row){ ?>
	    	<div class="grid_3">
	    	<?php if($row->picture != NULL)
                {
                    echo '<img src="'.base_url('assets/upload/'.$row->picture).'" width="350px" title="'.$row->judul.'"  />'; 
                } else {
                    echo '<img src="'.base_url('assets/upload/noimg.gif').'" />';
                }?>
	    	</div>
	    	<div class="grid_9">
	    	<h2><?php echo $row->judul ;?></h2>
	    	<p><?php echo word_limiter($row->konten, 25) ;?> <br/><?php echo anchor(BASE_URL.'news/detail/'.$row->id.'/'. url_title($row->judul),'(baca selengkapnya)') ;?></p>
	    	</div>
	    	<?php } ?>
	    	<div class="grid_12">
	    		<center><?php echo $this->pagination->create_links();?></center>
	    	</div>
		</div>                    
	</section>
