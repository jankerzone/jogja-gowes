<?php

class Admin extends Admin_Controller{
    
    
    public function __construct() {
        parent::__construct();
        $this->load->model('news_model');
    }
    
    private function upload_config()
    {
      $config['upload_path'] = './assets/upload/';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['max_size']   = '2048'; //satuan kilobytes
      $config['max_width']  = '';
      $config['max_height']  = '';
      $config['encrypt_name'] = TRUE;
      $this->upload->initialize($config);
    }

    public function index()
    {
        $this->news_model->order_by('id',$order = 'DESC');
        $data['newss']  = $this->news_model->get_all();

        $this->template->title('News Management')
                ->append_metadata(theme_js('forms.js'))
                ->build('admin/main',$data);
    }

    public function create()
    {
        $this->upload_config();
        $this->form_validation->set_rules('judul', 'Judul Berita', 'required|xss_clean');
        $this->form_validation->set_rules('konten', 'Konten Berita', 'required|xss_clean');
        $this->form_validation->set_rules('tanggal', 'Tanggal Publish', 'required|xss_clean');

        if($this->form_validation->run() == TRUE )
        {
            $unggah = $this->news_model->insert( $this->db->escape(array
                (
                'judul'     => $this->input->post('judul'),
                'konten'    => $this->input->post('konten'),
                'tanggal'   => $this->input->post('tanggal')
                ))
            );

            if ($this->upload->do_upload('userfile'))
            {
                $upload_data = $this->upload->data();
                $image= $upload_data['file_name'];            
                $this->news_model->update($unggah, array('picture'=>$image));            
            }

            $this->session->set_flashdata('response', show_msg('Input news berhasil disimpan!', 'success', '95%'));
            redirect(PANEL_URL.'/news');
        }         

        $data['news'] = '';        

        $this->template->title('Input News')
                ->build('admin/form',$data);         

    }

    public function delete($id){
        $this->news_model->delete($id);
        $this->session->set_flashdata(show_msg('News berhasil dihapus', 'success'));
        redirect(PANEL_URL.'/news');
    }

    public function edit($id){
        $id or redirect(''.PANEL_URL.'/users');
                
        $data['user'] = $this->news_model->get($id);

        $data['id'] = $id;
        
        $this->form_validation->set_rules('judul', 'Judul Berita', 'required|xss_clean');
        $this->form_validation->set_rules('konten', 'Konten Berita', 'required|xss_clean');
        $this->form_validation->set_rules('tanggal', 'Tanggal Publish', 'required|xss_clean');

        if ($this->form_validation->run() == TRUE ){

            $this->upload_config();

            if ( ! $this->upload->do_upload('userfile') )
            {
                   // $data['error'] = show_msg($this->upload->display_errors(),'danger');
                 $this->news_model->update($id, array(
                    'judul'     => $this->input->post('judul'),
                    'konten'    => $this->input->post('konten'),
                    'tanggal'   => $this->input->post('tanggal')
                   ));
                $this->session->set_flashdata('response',show_msg('Edit news berhasil disimpan!', 'success', '95%'));
                redirect(PANEL_URL.'/news');                 
            }
            else
            {
                unlink('./assets/upload/'.$data['user']->picture);

                $upload_data = $this->upload->data();
                $image = $upload_data['file_name'];
                
                $this->news_model->update($id, array(
                    'judul'     => $this->input->post('judul'),
                    'konten'    => $this->input->post('konten'),
                    'tanggal'   => $this->input->post('tanggal'),
                    'picture'   => $image,
                ));
                $this->session->set_flashdata('response',show_msg('Edit news berhasil disimpan!', 'success', '95%'));
                redirect(PANEL_URL.'/news'); 
            }
        } 
        
        $data['news'] = $this->news_model->get($id);

        $this->template->title('Edit News')
                ->build('admin/form',$data); 
        
        
    }
       
}


/* End of file admin.php */
/* Location: ./application/modules/news/controller/admin.php */
