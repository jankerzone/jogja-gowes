<script>


$(document).ready(function() {
    //tab tab

         $("#dettabs").tabs();       
});

</script>

<?php echo $msg; echo form_open(''.PANEL_URL.'/settings','class="form form-horizontal"')?>

<div id="dettabs">
        <ul>
	<li><a href="#general">General</a></li>
	<li><a href="#contact">Contact</a></li>
	<li><a href="#social">Social</a></li>
 
        </ul>
 
<div id="general">    
  <fieldset>
      <?php $generals = $this->setting_model->get_many_by('module','general'); ?>
      <?php foreach($generals as $general){?>
    <div class="control-group">
      <label class="control-label" for="<?php echo $general->name?>"><?php echo $general->title?></label>
      <div class="controls">
        <?php if($general->type=='text'){?>
          <input type="<?php echo $general->type?>" name="<?php echo $general->name?>" value="<?php echo $general->value?>" class="input-xlarge" id="<?php echo $general->name?>">
        <?php }elseif($general->type=='textarea'){?>
          <textarea name="<?php echo $general->name?>" id="<?php echo $general->name?>"><?php echo $general->value?></textarea>
          <?php }elseif($general->type=='radio' && $general->name=='frontend_enabled'){?>
          <input type="<?php echo $general->type?>" name="<?php echo $general->name?>" value="1" <?php if($general->value=='1') {echo'checked=""';} ?>  id="<?php echo $general->name?>"> Open
          <input type="<?php echo $general->type?>" name="<?php echo $general->name?>" value="0" <?php if($general->value=='0') {echo'checked=""';} ?> id="<?php echo $general->name?>"> Close         
          <?php }elseif($general->type=='radio' && $general->name=='currency'){?>
         <input type="<?php echo $general->type?>" name="<?php echo $general->name?>" value="Rp" <?php if($general->value=='Rp') {echo'checked=""';} ?>  id="<?php echo $general->name?>"> Rp
          <input type="<?php echo $general->type?>" name="<?php echo $general->name?>" value="USD" <?php if($general->value=='USD') {echo'checked=""';} ?> id="<?php echo $general->name?>"> USD         
          
          <?php }?>
        <p class="help-block"><?php echo $general->description?></p>
      </div>
    </div>     
      <?php }?>
      <div class="control-group"><label class="control-label">Backup Database</label>
      <div class="controls"><a href="<?php echo PANEL_URL ?>/settings/backup_db">Backup</a></div>
      </div>
  </fieldset>       
</div>
<div id="contact">
  <fieldset>
      <?php $contacts = $this->setting_model->get_many_by('module','contact'); ?>
      <?php foreach($contacts as $contact){?>
    <div class="control-group">
      <label class="control-label" for="<?php echo $contact->name?>"><?php echo $contact->title?></label>
      <div class="controls">
        <?php if($contact->type=='text'){?>
          <input type="<?php echo $contact->type?>" name="<?php echo $contact->name?>" value="<?php echo $contact->value?>" class="input-xlarge" id="<?php echo $contact->name?>">
        <?php }elseif($contact->type=='textarea'){?>
          <textarea name="<?php echo $contact->name?>" id="<?php echo $contact->name?>"><?php echo $contact->value?></textarea>
          <?php }?>
        <p class="help-block"><?php echo $contact->description?></p>
      </div>
    </div>     
      <?php }?>
  </fieldset>
</div>
<div id="social">
  <fieldset>
      <?php $socials = $this->setting_model->get_many_by('module','social'); ?>
      <?php foreach($socials as $social){?>
    <div class="control-group">
      <label class="control-label" for="<?php echo $social->name?>"><?php echo $social->title?></label>
      <div class="controls">
        <?php if($social->type=='text'){?>
          <input type="<?php echo $social->type?>" name="<?php echo $social->name?>" value="<?php echo $social->value?>" class="input-xlarge" id="<?php echo $social->name?>">
        <?php }elseif($social->type=='textarea'){?>
          <textarea rows="10" class="span9" name="<?php echo $social->name?>" id="<?php echo $social->name?>"><?php echo $social->value?></textarea>
          <?php }?>
        <p class="help-block"><?php echo $social->description?></p>
      </div>
    </div>     
      <?php }?>
  </fieldset>
</div>
  
    
</div><br/>
<button class="btn btn-primary">Save</button>
    
<?php echo form_close()?>