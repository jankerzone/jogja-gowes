<?php


class Admin extends Admin_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('setting_model');
        $this->load->library('settings');
    }
    
    
    public function index(){
        $data['msg']='';
        
        
        $settings = $this->setting_model->get_many_by(array('is_gui'=>1));

        if($this->input->post()){
        foreach ($settings as $setting)
        {                
                $settings_stored[$setting->name] = $setting->value;
        }
        
        // Loop through again now we know it worked
        foreach ($settings_stored as $name => $stored_value)
        {
                $input_value = $this->input->post($name, FALSE);

                if (is_array($input_value))
                {
                        $input_value = implode(',', $input_value);
                }

                // Dont update if its the same value
                if ($input_value !== $stored_value && $name !== 'bg_image' && $name !== 'bg_flash')
                {
                        $this->settings->set_item($name, $input_value);
                }
        }
        $data['msg'] = show_msg('Setting Was updated successfully','success', '90%');
        }
      
         
         
        
        $this->template->title('Settings')
                ->build('admin/main',$data);
    }
    
    public function ajax($action)
    {
        $moreops = '';
        
        switch ($action) {
            //uploading logo
            case 'upload_bg':
                    $status = ''; $msg = ''; $code = '';

                    $config['upload_path'] = './uploads/slides';
                    $config['allowed_types'] = 'gif|jpg|png|swf';                   
                    $config['overwrite'] = TRUE;
                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('userfile'))
                    {
                            $status = 'error';
                            $msg = $this->upload->display_errors('', '');
                    }
                    else
                    {
                            $data = $this->upload->data();
                            $status = "success";
                            if($data['file_ext']=='.swf'){
                                $this->setting_model->update('bg_flash',array('value' => $data['file_name']));
                            }else{
                                $this->setting_model->update('bg_image',array('value' => $data['file_name']));
                            }
                            //$code = '$("#current_bg").html("blalbal");'; gak jalan
                            //save the activity to db
                           
              
                    }
                    @unlink($_FILES['userfile']);
                   
                    $moreops .= json_encode(array('status' => $status, 'msg' => $msg, 'code'=>$code));

                break;

            default:
                break;
        }die($moreops);
        
    }

    function backup_db()
    {
// Load the DB utility class
$datestring = "%d-%m-%Y";

$this->load->dbutil();
// Backup your entire database and assign it to a variable
$prefs = array(
                'tables'      => array(),  // Array of tables to backup.
                'ignore'      => array(),           // List of tables to omit from the backup
                'format'      => 'zip',             // gzip, zip, txt
                //'filename'    => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );
$backup =& $this->dbutil->backup($prefs); 

//$this->load->helper('download');
//force_download('mybackup.zip', $backup);
$this->load->helper('file');
write_file( 'assets/backup/mybackup-'.mdate($datestring).'.zip', $backup); 



        redirect(PANEL_URL . '/settings', 'refresh');
    }
    
}

?>
