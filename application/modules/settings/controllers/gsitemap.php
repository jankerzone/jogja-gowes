<?php

class Gsitemap extends Admin_Controller{
	
    public function __construct(){
    parent::__construct();
    $this->load->helper('google_sitemap'); //Load Plugin
    $this->load->model('news/news_model');
    $this->load->model('pages/page_model');
    }
	
    public function index()
    {
        //$this->session->set_flashdata('response', show_msg('The Google Sitemap was Generated successfully!', 'success', '90%'));
        $sitemap = new google_sitemap; //Create a new Sitemap Object
        
        /*$item = new google_sitemap_item(base_url()."",date("Y-m-d"), 'weekly', '0.8' ); //Create a new Item
        $item1 = new google_sitemap_item(base_url()."product",date("Y-m-d"), 'weekly', '0.8' );
        $item2 = new google_sitemap_item(base_url()."cart",date("Y-m-d"), 'weekly', '0.8' );
        $sitemap->add_item($item); //Append the item to the sitemap object 
        $sitemap->add_item($item1);
        $sitemap->add_item($item2);
        */
        
        //$items = array();
        $pages = $this->page_model->get_all();
        foreach($pages as $page){
                    if($page->link_to == 'default')
                    {
                            $link = base_url().'page/'.$page->id.'/'.$page->uri.'/';
                    }
                    elseif($page->link_to == 'url')
                    {
                        $link = 'http://'.$page->uri;
                    }
                    elseif($page->link_to == 'maps'){
                       $link = base_url().''.$page->uri.'/index/'.$page->id.'';   
                    }
                    else
                    { 
                        $link = base_url().''.$page->uri.'';
                    }            
            $item = new google_sitemap_item(base_url().$link,date("Y-m-d"), 'weekly', '0.8' ); //Create a new Item
            $sitemap->add_item($item); 
        }
        
        $news = $this->news_model->get_all();
        
        foreach($news as $row){
            $item = new google_sitemap_item(base_url().'news/detail/'.$row->id.'/'.$row->uri,date("Y-m-d"), 'weekly', '0.8' ); //Create a new Item
            $sitemap->add_item($item);     
        }
        
        $sitemap->build("./sitemap.xml"); //Build it...

         
        
         //Let's compress it to gz
        $data = implode("", file("./sitemap.xml"));
        $gzdata = gzencode($data, 9);
        $fp = fopen("./sitemap.xml.gz", "w");
        fwrite($fp, $gzdata);
        fclose($fp);

        //Let's Ping google
        $this->_pingGoogleSitemaps(base_url()."/sitemap.xml.gz");
        
       // redirect(PANEL_URL.'/settings/#sitemap');
         $url = PANEL_URL.'/settings/#sitemap';
        echo'
        <script>
        window.location.href = "'.base_url().''.$url.'";
        </script>
        ';
        
    }

    public function _pingGoogleSitemaps( $url_xml )
    {
       $status = 0;
       $google = 'www.google.com';
       if( $fp=@fsockopen($google, 80) )
       {
          $req =  'GET /webmasters/sitemaps/ping?sitemap=' .
                  urlencode( $url_xml ) . " HTTP/1.1\r\n" .
                  "Host: $google\r\n" .
                  "User-Agent: Mozilla/5.0 (compatible; " .
                  PHP_OS . ") PHP/" . PHP_VERSION . "\r\n" .
                  "Connection: Close\r\n\r\n";
          fwrite( $fp, $req );
          while( !feof($fp) )
          {
             if( @preg_match('~^HTTP/\d\.\d (\d+)~i', fgets($fp, 128), $m) )
             {
                $status = intval( $m[1] );
                break;
             }
          }
          fclose( $fp );
       }
       return( $status );
    }
	
}


?>