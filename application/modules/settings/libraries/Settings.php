<?php


class Settings {
    protected $ci;
    private static $cache = array();
    private $columns = array('id', 'name', 'title', 'description', 'type', 'value', 'options', 'module');
    
    public function __construct()
    {
            ci()->load->model('settings/setting_model');

            $this->ci =& get_instance();

            $this->get_all();
    }    
    
    	/**
	 * Getter
	 *
	 * Gets the setting value requested
	 *
	 * @param	string	$name
	 */
	public function __get($name)
	{
		return self::get($name);
	}

	/**
	 * Setter
	 *
	 * Sets the setting value requested
	 *
	 * @param	string	$name
	 * @param	string	$value
	 * @return	bool
	 */
	public function __set($name, $value)
	{
		return self::set($name, $value);
	}

	/**
	 * Get
	 *
	 * Gets a setting.
	 *
	 * @param	string	$name
	 * @return	bool
	 */
	public static function get($name)
	{
		if (isset(self::$cache[$name]))
		{
			return self::$cache[$name];
		}

		$setting = ci()->setting_model->get_by(array('name' => $name));

		// Setting doesn't exist, maybe it's a config option
		$value = $setting ? $setting->value : config_item($name);

		// Store it for later
		self::$cache[$name] = $value;

		return $value;
	}    
    
	/**
	 * Set
	 *
	 * Sets a config item
	 *
	 * @param	string	$name
	 * @param	string	$value
	 * @return	bool
	 */
	public static function set($name, $value)
	{
		if (is_string($name))
		{
			if (is_scalar($value))
			{
				$setting = ci()->setting_model->update($name, array('value' => $value));
			}

			self::$cache[$name] = $value;

			return TRUE;
		}

		return FALSE;
	}    
    
    	public function item($name)
	{
		return $this->__get($name);
	}

    	/**
	 * Set Item
	 *
	 * Old way of getting an item.
	 * @deprecated	v1.0	Use either __set or Settings::set() instead
	 * @param	string	$name
	 * @param	string	$value
	 * @return	bool
	 */
	public function set_item($name, $value)
	{
		return $this->__set($name, $value);
	}
    
    public function get_all()
    {
            if (self::$cache)
            {
                    return self::$cache;
            }

            $settings = ci()->setting_model->get_many_by(array());

            foreach ($settings as $setting)
            {
                    self::$cache[$setting->name] = $setting->value;
            }

            return self::$cache;
    }
    
	/**
	 * Add Setting
	 *
	 * Adds a new setting to the database
	 *
	 * @param	array	$setting
	 * @return	int
	 */
	public function add($setting)
	{
		if ( ! $this->_check_format($setting))
		{
			return FALSE;
		}
		return ci()->settings_model->insert($setting);
	}
    
    
}

?>
