<?php

class Users extends Auth_Controller{
    public function __construct() {
        parent::__construct();

       
    }   
    
	//log the user in
	public function index()
	{      
		$data['title'] = "Login";

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{ //check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{ //if the login is successful
				//redirect them back to the home page
				if( $this->ion_auth->in_group(array('root'))){
				$this->session->set_flashdata('response', show_msg($this->ion_auth->messages()));
				
                                redirect(''.PANEL_URL.'', 'refresh');
				} 
				else
				{
					redirect('home','refresh');
				}
			}
			else
			{ //if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('response', show_msg($this->ion_auth->errors(),'danger'));
				redirect('users', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{  //the user is not logging in so display the login page
			//set the flash data error message if there is one
			$data['message'] = show_msg((validation_errors()) ? validation_errors() : $this->session->flashdata('message'),'danger');

			$data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
                                'class'=>'span3',
                                'placeholder'=>'Username/Email',
				'value' => $this->form_validation->set_value('identity'),
			);
			$data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
                                'class'=>'span3',
                                'placeholder'=>'Password',
			);

			$this->load->view('login', $data);
		}
	}    
        
        
  	//forgot password
	function forgot_password($rules='')
	{
                $this->data['rules'] = $rules;
		$this->form_validation->set_rules('email', 'Email Address', 'required');
		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);
			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->template->title('Forgot password');
                        $this->template->build('forgot_password',$this->data);
                        //$this->load->view('auth/forgot_password', $this->data);
		}
		else
		{
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));

			if ($forgotten)
			{ //if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				if($this->input->post('rules')=='customer'){
                                redirect("customer/login", 'refresh'); //we should display a confirmation page here instead of the login page
                                }else{
                                redirect("users",'refresh');
                                }
                                
                        }
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("customer/forgot_password", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code)
	{
		$reset = $this->ion_auth->forgotten_password_complete($code);

		if ($reset)
		{  //if the reset worked then send them to the login page
			$this->session->set_flashdata('response', $this->ion_auth->messages());
			redirect("users", 'refresh');
		}
		else
		{ //if the reset didnt work then send them back to the forgot password page
			$this->session->set_flashdata('response', $this->ion_auth->errors());
			redirect("customer/forgot_password", 'refresh');
		}
	}
        
        
	//activate the user
	function activate($role, $id, $code=false)
	{
		if ($code !== false)
			$activation = $this->ion_auth->activate($id, $code);
		else if ($this->ion_auth->is_admin())
			$activation = $this->ion_auth->activate($id);

		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
                        if($role == 'customer'){
			redirect("customer", 'refresh');
                        }elseif($role == 'admin'){
                            redirect(PANEL_URL, 'refresh');
                        }
		}
		else
		{
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			if($role == 'customer'){
			redirect("customer/forgot_password", 'refresh');
                        }elseif($role == 'admin'){
                            redirect("users/forgot_password", 'refresh');
                        }
		}
	}
        
        public function logout(){
            $this->ion_auth->logout();
            $this->session->set_flashdata('response', show_msg($this->ion_auth->errors(),'danger'));
            redirect('users');
        }
}

?>
