<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * NPHS Team
 * location: module/users/controllers/admin
 */
class Admin extends Admin_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper('date');
        $this->load->model('user_model');
        $this->load->model('group_model');
    }
    
    public function index(){
       $data['msg'] = '';

        if($this->input->post('suredelete') == 1)
        {
                $count_deteted = 0;

                foreach ($this->input->post() as $key=>$val) 
                {
                        if (substr($key, 0, 8) == 'checked_') 
                        {
                                if($this->ion_auth->delete_user($val))
                                {
                                        $count_deteted++;
                                }
                        }
                }


                if ($count_deteted == 1)
                {
                        $data['msg'] = show_msg('1 row is deleted!', 'success');
                }
                elseif ($count_deteted > 1)
                {
                        $data['msg'] = show_msg($count_deteted . ' rows are deleted!', 'success');
                }
                else
                {
                        $data['msg'] = show_msg('Select at least one row!', 'info');
                }
        }               
        
        $data['users'] = $this->user_model->get_is_admin();
        foreach ($data['users'] as $k => $user)
        {
        $data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
        }
        
        $this->template->title('Users management')
                ->append_metadata(theme_js('forms.js'))
                ->build('admin/users',$data);
    
    }
    
	//create a new user
	function create_user()
	{
		$data['title'] = "Create User";

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('', 'refresh');
		}

		//validate form input
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
                //$this->form_validation->set_rules('group', 'Group', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$additional_data = array('first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name')
			);
                        
                        $group = array('1'); // Sets user to admin. No need for array('1', '2') as user is always set to member by default
		}
		if ($this->form_validation->run() == true && $id=$this->ion_auth->register($username, $password, $email, $additional_data, $group))
		{ //check to see if we are creating the user
			//redirect them back to the admin page
                  
                   if($_POST['permissions']){
                                                
                        foreach ($_POST['permissions'] as $key => $value) {                                           
                            $this->permission_model->insert( $this->db->escape(
                             array('user_id'=>$id,'module_id'=>$value))
                             ); 
                        }
                    }            
                    
			$this->session->set_flashdata('response', show_msg("User Created"));
			redirect("".PANEL_URL."/users", 'refresh');
		}
		else
		{ //display the create user form
			//set the flash data error message if there is one
			$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('response')));

			$data['first_name'] = array('name' => 'first_name',
				'id' => 'first_name',
				'type' => 'text',
                                'placeholder'=>'First Name',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$data['last_name'] = array('name' => 'last_name',
				'id' => 'last_name',
				'type' => 'text',
                                'placeholder'=>'Last Name',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$data['email'] = array('name' => 'email',
				'id' => 'email',
				'type' => 'text',
                                'placeholder'=>'Email',
				'value' => $this->form_validation->set_value('email'),
			);
			$data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
                                'placeholder'=>'Password',
				'value' => $this->form_validation->set_value('password'),
			);
			$data['password_confirm'] = array('name' => 'password_confirm',
				'id' => 'password_confirm',
				'type' => 'password',
                                'placeholder'=>'Password Confirm',
				'value' => $this->form_validation->set_value('password_confirm'),
			);

                         $data['module'] = $this->module_model->get_many_by(array('in_admin'=>1,'parent_id'=>0));
			$this->template->title('Create user')
                                ->build('admin/create_user',$data);
                                ;
		}
	}
        
        
        
        function update_user($id){
   
            $id or redirect(''.PANEL_URL.'/users');
                
                $user = $this->user_model->get($id);
                
		$data['id'] = $id;
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('', 'refresh');
		}

		//validate form input
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		if($this->input->post('password')){
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
                }
		if ($this->form_validation->run() == true)
		{
                    if($this->input->post('password')){
                    $data_user=array(
                        'first_name'=>$this->input->post('first_name'),
                        'last_name'=>$this->input->post('last_name'),
                        'email'=>$this->input->post('email'),
                        'password'=>$this->input->post('password')        
                      );
                    }else{
                    $data_user=array(
                        'first_name'=>$this->input->post('first_name'),
                        'last_name'=>$this->input->post('last_name'),
                        'email'=>$this->input->post('email')       
                      );                        
                    }
                    
                }
		if ($this->form_validation->run() == true && $this->ion_auth->update($id, $data_user))
		{ //check to see if we are creating the user
			//redirect them back to the admin page
                    
                    if($_POST['permissions']){
                        
                        $this->permission_model->delete_by(array('user_id'=>$id));
                        
                        foreach ($_POST['permissions'] as $key => $value) {                                           
                            $this->permission_model->insert(
                             array('user_id'=>$id,'module_id'=>$value)
                             ); 
                        }
                    }

                                                           
                    
			$this->session->set_flashdata('response', show_msg("User was successfully Updated"));
			redirect("".PANEL_URL."/users", 'refresh');
		}
		else
		{ //display the create user form
			//set the flash data error message if there is one
			$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('response')));

			$data['first_name'] = array('name' => 'first_name',
				'id' => 'first_name',
				'type' => 'text',
                                'placeholder'=>'First Name',
				'value' => $this->form_validation->set_value('first_name',$user->first_name),
			);
			$data['last_name'] = array('name' => 'last_name',
				'id' => 'last_name',
				'type' => 'text',
                                'placeholder'=>'Last Name',
				'value' => $this->form_validation->set_value('last_name',$user->last_name),
			);
			$data['email'] = array('name' => 'email',
				'id' => 'email',
				'type' => 'text',
                                'placeholder'=>'Email',
				'value' => $this->form_validation->set_value('email',$user->email),
			);
			$data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
                                'placeholder'=>'Password',
				'value' => $this->form_validation->set_value('password'),
			);  
                        $data['module'] = $this->module_model->get_many_by(array('in_admin'=>1,'parent_id'=>0));
			$this->template->title('update user')
                                ->build('admin/update_user',$data);
                                ;
		}            
            
        }
        
//change password
	function change_mypassword()
	{
		$this->form_validation->set_rules('old', 'Old password', 'required');
		$this->form_validation->set_rules('new', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', 'Confirm New Password', 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
		
		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{ //display the form
			//set the flash data error message if there is one
			$data['msg'] = (validation_errors()) ? show_msg(validation_errors(), 'danger') : $this->session->flashdata('message');

			$data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$data['new_password'] = array(
				'name' => 'new',
				'id'   => 'new',
				'type' => 'password',
			);
			$data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id'   => 'new_confirm',
				'type' => 'password',
			);
			$data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->template->title('Change My Password')
                                ->build('admin/change_password',$data);
		}
		else
		{
			$identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{ //if the password was successfully changed
				$this->session->set_flashdata('response', show_msg($this->ion_auth->messages()));
				$this->ion_auth->logout();
                                redirect(''.PANEL_URL.'/users/', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('message', show_msg($this->ion_auth->errors()));
				redirect(''.PANEL_URL.'/users/change_mypassword', 'refresh');
			}
		}
	}
        
    
	//activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
			$activation = $this->ion_auth->activate($id, $code);
		else if ($this->ion_auth->is_admin())
			$activation = $this->ion_auth->activate($id);

		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('response', show_msg($this->ion_auth->messages()));
			redirect("".PANEL_URL."/users", 'refresh');
		}
		else
		{
			//redirect them to the forgot password page
			$this->session->set_flashdata('response', show_msg($this->ion_auth->errors()));
			redirect("auth/forgot_password", 'refresh');
		}
	}

	//deactivate the user
	function deactivate($id = NULL)
	{
		// no funny business, force to integer
		$id = (int) $id;
                // do we have the right userlevel?
                if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
                {
                        $this->ion_auth->deactivate($id);
                        $this->session->set_flashdata('response', show_msg('Account is Inactive'));
                }
                //redirect them back to the auth page
                 redirect(''.PANEL_URL.'/users', 'refresh');
		
	}    
        
    
}


?>
