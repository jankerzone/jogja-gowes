<?php echo $message;?>
<?php echo form_open("".PANEL_URL."/users/update_user/".$id."","class='form form-horizontal'");?>    
<fieldset>

    <div class="control-group">
    <label class="control-label" for="first_name">First Name</label>
    <div class="controls">
        <?php echo form_input($first_name);?>
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="last_name">Last Name</label>
    <div class="controls">
        <?php echo form_input($last_name);?>
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="email">Email</label>
    <div class="controls">
        <?php echo form_input($email);?>
    </div>
    </div>   
    <div class="control-group">
    <label class="control-label" for="password">Password</label>
    <div class="controls">
        <?php echo form_input($password);?>
    </div>
    </div>        
     <div class="control-group">
            <label class="control-label" for="inlineCheckboxes">Access Permissions</label>
            <div class="controls">
                <?php foreach ($module as $row){ $current = $this->permission_model->get_by(array('module_id'=>$row->id,'user_id'=>$id))?>
              <label class="checkbox inline">
                  <input type="checkbox" name="permissions[]" <?php if(isset($current->module_id)){ if($row->id==$current->module_id){echo 'checked=""';} }?>   value="<?php echo $row->id?>"> <?php echo $row->name?>
              </label>
                <?php }?>
             <input type="hidden" name="permissions[]"value="" /> 
            </div>
          </div>
            <div class="form-actions">
            <button type="submit" class="btn btn-primary">Save</button>
          </div> 
</fieldset>

<?php echo form_close()?>