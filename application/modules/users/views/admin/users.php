<?php echo $msg?>
<a href="<?php echo site_url(''.PANEL_URL.'/users/create_user');?>" class="btn btn-primary">Create a new user</a>	<br/><br/>   
 <?php echo form_open(''.PANEL_URL.'/users'); ?>
<input type='hidden' name='suredelete' value='0' />
<table class="table table-striped table-bordered">
        <thead>
		<tr>
                    <th width="5%"><center><input type='checkbox' name='checkall' onclick='CheckAll(this.form);' /></center></th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email</th>
                        <th>Created On</th>
                        <th>Group</th>
			<th>Status</th>
                        <th></th>
		</tr>
        </thead>
        <tbody>
		<?php foreach ($users as $user):?>
			<tr>
                            <td>
                            <center>
                            <input type="checkbox" name="checked_<?php echo  $user->id; ?>" value="<?php echo  $user->id; ?>" />
                            </center> 
                            </td>
				<td><?php echo $user->first_name;?></td>
				<td><?php echo $user->last_name;?></td>
				<td><?php echo $user->email;?></td>				
                                <td><?php echo format_date($user->created_on)?></td>
                                <td><?php echo $this->group_model->get($user->group_id)->description;?></td>
				<td><?php echo ($user->active) ? anchor("".PANEL_URL."/users/deactivate/".$user->id, 'Active') : anchor("".PANEL_URL."/users/activate/". $user->id, 'Inactive');?></td>
                                <td><?php echo anchor(''.PANEL_URL.'/users/update_user/'.$user->id.'','Edit','class=btn btn-primary');?></td>
                        </tr>
		<?php endforeach;?>
        </tbody>
	</table>
    <div class="form-actions">
            <button class="btn btn-danger" onclick="ConfirmDelete(this.form, 'Are you sure you want to delete this? It cannot be undone!');">Delete selected</button>
    </div>
<?php echo form_close()?>