<?php echo $message;?>
<?php echo form_open("".PANEL_URL."/users/create_user","class='form form-horizontal'");?>    
<fieldset>
        
    
    <div class="control-group">
    <label class="control-label" for="first_name">First Name</label>
    <div class="controls">
        <?php echo form_input($first_name);?>
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="last_name">Last Name</label>
    <div class="controls">
        <?php echo form_input($last_name);?>
    </div>
    </div>
    <div class="control-group">
    <label class="control-label" for="email">Email</label>
    <div class="controls">
        <?php echo form_input($email);?>
    </div>
    </div>   
    <div class="control-group">
    <label class="control-label" for="password">Password</label>
    <div class="controls">
        <?php echo form_input($password);?>
    </div>
    </div>     
   <div class="control-group">
    <label class="control-label" for="password_confirm">Password Confirm</label>
    <div class="controls">
        <?php echo form_input($password_confirm);?>
    </div>
    </div>    
    
        <div class="control-group">
            <label class="control-label" for="inlineCheckboxes">Module Access Permissions</label>
            <div class="controls">
                <?php foreach ($module as $row){ ?>
              <label class="checkbox inline">
                  <input type="checkbox" id="inlineCheckbox1" name="permissions[]" value="<?php echo $row->id?>"> <?php echo $row->name?>
              </label>
                <?php }?>
            </div>
          </div>
   

    
    
            <div class="form-actions">
            <button type="submit" class="btn btn-primary">Save</button>
          </div> 
</fieldset>

<?php echo form_close()?>


<script>
      $(".group").bind("change", function () { 
          if ($(this).val() == "5") {
          $("#admin").slideUp();
          $("#unit").slideDown(); 
          }else if($(this).val() == "1"){
          $("#unit").slideUp(); 
          $("#admin").slideDown()
          }
      })
      
      
</script>