<div class="page">
 
        <div id="title-page">
        <h1>FORGOT PASSWORD</h1>
    </div>
    
<p>Please enter your email address so we can send you an email to reset your password.</p>
<?php echo $message;?>

<?php echo form_open("users/forgot_password");?>

      <p>Email Address:<br />
      <?php echo form_input($email);?>
      </p>
      <input type="hidden" name="rules" value="<?php echo $rules?>"/>
      <p><?php echo form_submit('submit', 'Submit','class="btn btn-primary"');?></p>
      
<?php echo form_close();?>
    
</div>