<!DOCTYPE html>
<html>
    <head>
        <title>Login Dashboard</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <?php echo theme_css('bootstrap.css')?>
        <?php echo css('login.css','users')?>
    </head>
    <body>
        <div class="container">
        <div class="offset4 span4" style="margin-top: 120px;">
            <center><a href="<?php echo BASE_URL() ;?>"><img src="<?php echo site_url('assets') ;?>/images/logo-gowes4.svg" alt="logo"></a></center><br/><br/><br/>
            <div class="col"> 
            <div class="top_col">                
                <h3>Login to Dashboard</h3>
            </div>
        <?php echo form_open("users","class='form form-vertical'");?>
        <?php  echo $this->session->flashdata('response'); echo $message; ?>
       <div class="control-group">
           <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
           <?php echo form_input($identity);?>
                </div>
           </div>
       </div>
    <div class="control-group">
           <div class="controls">
                <div class="input-prepend">
                     <span class="add-on"><i class="icon-lock"></i></span>
            <?php echo form_input($password);?>
                </div>
           </div></div>
                
                <input  class="btn btn-warning" type="submit" name="submit" value="Login"/>
           
                <div style="float: right">
                    <?php echo anchor('users/forgot_password','Lost Password ?')?>
                </div>
   <?php echo form_close()?>
            </div><br/>
        </div>
            <div class="offset5 span3 powered">
                Portal JogjaGowes &copy; 2014
          </div>            
    </div>
    </body>
</html>




