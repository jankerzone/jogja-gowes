<?php

class Permission_model extends MY_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function get_menu_item($attr=array()){
        $user = $this->ion_auth->user()->row();
        $this->db->select('modules.*, modules.id as module_id, permissions.*, permissions.id as permissions_id, admin_menus.*, admin_menus.id as menu_id');
        $this->db->where('user_id',$user->id);
        $this->db->group_by("admin_menus.menu"); 
        $this->db->join('modules','modules.id = permissions.module_id');
        $this->db->join('admin_menus','admin_menus.id = modules.on_menu');
        return $this->db->get('permissions')->result();
        
    }
    
 
    
    public function get_module($on_menu=FALSE){
        $user = $this->ion_auth->user()->row();
        $this->db->select('modules.*, modules.id as module_id, permissions.*, permissions.id as permissions_id, admin_menus.*, admin_menus.id as menu_id');
        $this->db->where('user_id',$user->id);
        $this->db->where('on_menu',$on_menu);
        $this->db->join('modules','modules.id = permissions.module_id');
        $this->db->join('admin_menus','admin_menus.id = modules.on_menu');
        return $this->db->get('permissions')->result();
        
    }
    
    public function cek_permission($uri){
        $user = $this->ion_auth->user()->row();
        $this->db->select('modules.*, modules.id as module_id, permissions.*, permissions.id as permissions_id, admin_menus.*, admin_menus.id as menu_id');
        $this->db->where('user_id',$user->id);
        $this->db->where('modules.uri',$uri);
        $this->db->join('modules','modules.id = permissions.module_id');
        $this->db->join('admin_menus','admin_menus.id = modules.on_menu');
        return $this->db->get('permissions')->row_array();        
    }
    
    
 
    
}

?>
