<?php

class User_model extends MY_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function get_is_admin(){
        $this->db->select('users.*, users_groups.user_id, users_groups.group_id, users_groups.id as idg');
        $this->db->order_by('id','desc');
        $this->db->join('users_groups','users_groups.user_id = users.id');
     
        $this->db->where('users_groups.group_id','1');
        $this->db->or_where('users_groups.group_id','5');
        $q = $this->db->get('users');
        return $q->result();
    }
    
   
    
}
?>
