<?php

class Event extends Base_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('event_model');

    }
    
    public function detail($id)
    {
        $data['id'] = $id;
        
        $data['tagline']        = 'Event';
        $data['event']          = $this->event_model->get($id);

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('Event :: Portal Komunitas Jogja Gowes')
                       ->build('detail_event', $data);       
    }
}

?>
