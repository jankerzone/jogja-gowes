<?php

class News extends Base_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('news_model');

        $config['base_url'] = base_url('news/hal/');
        $config['total_rows'] = 100;
        $config['per_page'] = 5; 
        $this->pagination->initialize($config); 

    }
    
    public function index()
    {
    	$data['tagline']	= 'News';
        $this->news_model->limit(5,0);
        $data['news']		= $this->news_model->get_all();

        
        $this->template->title('News :: Portal Komunitas Jogja Gowes')
					   ->build('news', $data);
    }

    public function hal($id = '')
    {
        $id = $this->uri->segment(3);

        $data['tagline']    = 'News Halaman '.$id;

        $this->news_model->limit(5,$id);
        $data['news']       = $this->news_model->get_all();
        
        $this->template->title('News :: Portal Komunitas Jogja Gowes')
                       ->build('news', $data);
    }

    public function detail($id)
    {
        $data['id'] = $id;
        
        $data['tagline']    = 'News';
        $data['news']       = $this->news_model->get($id);

        $this->news_model->order_by('id', 'DESC');
        $this->news_model->limit(5,0);
        $data['terkait']    = $this->news_model->get_all();

        $this->template->title('News :: Portal Komunitas Jogja Gowes')
                       ->build('detail_news', $data);       
    }
}

?>
