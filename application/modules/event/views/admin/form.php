<?php if (isset ($id)){echo form_open_multipart(''.PANEL_URL.'/event/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.PANEL_URL.'/event/create','class="form form-horizontal"');}?>
   
    <script>
    $(function() {
         $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
    });
    </script>

    <fieldset>
    
        <div class="control-group">
        <label class="control-label" for="judul">Judul Event</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="judul" name="judul" value="<?php echo set_value('judul', $event?$event->judul:''); ?>">
             <?php echo form_error('judul'); ?>
        </div>     
        </div>
        
        <div class="control-group">
        <label class="control-label" for="konten">Konten</label>
        <div class="controls"><textarea id="konten" style="height:200px; width:70%;" name="konten"><?php echo set_value('konten', $event?$event->konten:''); ?></textarea>
             <?php echo form_error('konten'); ?>
        </div>     
        </div>        
        
        <div class="control-group">
        <label class="control-label" for="tanggal">Tanggal Publish</label>
        <div class="controls">
            <input type="text" class="input-small" id="datepicker" name="tanggal" value="<?php if(isset($id)){ echo set_value('tanggal', $event?$event->tanggal:''); } else { $datestring = "%d-%m-%Y"; echo mdate($datestring); }; ?>">
             <?php echo form_error('tanggal'); ?>
        </div>     
        </div> 
        <?php if ( isset ($event->picture))
            {
                $url = base_url('assets/upload/'.$event->picture);
            } 
            else
            {
                $url = base_url('assets/upload/noimg.gif');
            } 
            ?>
        <div class="control-group">
        <label class="control-label" for="picture">Picture</label>
        <div class="controls">
        <img src="<?php echo $url ?>" title="preview" width="250px"> <br/>
           <input type="file" name="userfile"/>                
         <?php echo form_error('picture'); ?>
        </div>     
        </div> 
        
        <div class="form-actions">
            <button class="btn btn-success" value="submit">Simpan</button>
        </div>
        
    </fieldset>

<?php echo form_close()?>

