<?php echo anchor(PANEL_URL.'/event/create','Tambah Event','class="btn btn-primary"')?><br/><br/>
<table class="table table-striped table-bordered">
        <thead>
		<tr>
            <th>Judul Event</th>
			<th>Konten</th>
			<th>Publish</th>
            <th>Aksi</th>
		</tr>
        </thead>
        <tbody>
            <?php foreach($event as $row){?>
            <tr>
                <td width="100px"><b><?php echo $row->judul?></b></td>
                <td><?php if($row->picture != NULL)
                {
                    echo '<img src="'.base_url('assets/upload/'.$row->picture).'" width="350px" />'; 
                } else {
                    echo '<img src="'.base_url('assets/upload/noimg.gif').'" />';
                }?>
                <br/> <?php echo nl2br($row->konten)?></td>
                <td width="100px"><?php echo $row->tanggal?></td>
                <td width="120px"><?php echo anchor(PANEL_URL.'/event/edit/'.$row->id,'Edit','class="btn btn-warning"')?> 
                <?php echo anchor(PANEL_URL.'/event/delete/'.$row->id,'Hapus','class="btn btn-danger"')?></td>
            </tr>
            <?php }?>
        </tbody>
        
</table>       