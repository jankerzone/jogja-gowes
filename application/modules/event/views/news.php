<h3>Berita terkait seputar Jogja-Gowes</h3>
<!-- content area -->    
	<section id="content">
    	<div class="clearfix">
	    	<?php foreach($news as $row){ ?>
	    	<div class="grid_3">
	    	<img src="<?php echo BASE_URL."assets/upload/".$row->picture ?>" alt="<?php echo $row->judul ?>"/>
	    	</div>
	    	<div class="grid_9">
	    	<h2><?php echo $row->judul ;?></h2>
	    	<p><?php echo word_limiter($row->konten, 25) ;?> <br/><?php echo anchor(BASE_URL.'news/detail/'.$row->id.'/'. url_title($row->judul),'(baca selengkapnya)') ;?></p>
	    	</div>
	    	<?php } ?>
	    	<div class="grid_12">
	    		<center><?php echo $this->pagination->create_links();?></center>
	    	</div>
		</div>                    
	</section>
