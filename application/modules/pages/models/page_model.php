<?php

class Page_model extends MY_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function get_page($id=''){
        $this->db->where('id',$id);
        $q = $this->db->get('pages');
        return $q->row_array();
    }
    
    
    public function get_all($param=FALSE) {
        $this->db->order_by('set_order','asc');
        $param;
        $q = $this->db->get('pages');        
        return $q->result();        
    }
    
    
    
    /**
     * Updates
     */
    
    public function get_page_tree()
    {
            $all_pages = $this->db
                    ->select('*')
                     ->order_by('`set_order`')
                     ->get('pages')
                     ->result_array();

            // we must reindex the array first
            foreach ($all_pages as $row)
            {
                    $pages[$row['id']] = $row;
            }

            unset($all_pages);

            // build a multidimensional array of parent > children
            if(isset($pages)){
            foreach ($pages as $row)
            {
                    if (array_key_exists($row['parent_id'], $pages))
                    {
                            // add this page to the children array of the parent page
                            $pages[$row['parent_id']]['children'][] =& $pages[$row['id']];
                    }

                    // this is a root page
                    if ($row['parent_id'] == 0)
                    {
                            $page_array[] =& $pages[$row['id']];
                    }
            }
           
            return $page_array; }
    }    
    
    	/*
         * Updates
         */
	public function _set_children($page)
	{
		if (isset($page['children']))
		{
			foreach ($page['children'] as $i => $child)
			{
				$this->db->where('id', str_replace('page_', '', $child['id']));
				$this->db->update('pages', array('parent_id' => str_replace('page_', '', $page['id']), '`set_order`' => $i));
				
				//repeat as long as there are children
				if (isset($child['children']))
				{
					$this->_set_children($child);
				}
			}
		}
	}
    

    /*Oringal OLD*/
    public function get_parent_menu($param=FALSE){
        $this->db->order_by('set_order','asc');
        $this->db->where('status',1);
        $param;
        $q = $this->db->get('pages');
        return $q->result_array(); 
    }
    
    public function get_submenu($parent_id){
        $this->db->order_by('set_order','asc');
        $this->db->where('status',1);
        $this->db->where('parent_id',$parent_id);
        $q = $this->db->get('pages');
        return $q->result_array(); 
    }
    
    /*for page manage v.0.1*/
    
     function get_tableparent($id='', $status='')
    {
        $this->db->order_by('set_order','asc');
    
        $query = $this->db->get('pages');
        return $query->result();
    }    
    
    function get_go_table($status='', $id='')
    {
    	$go_page = $this->get_tableparent($id, $status);
		foreach($go_page as $gp)
		{
			$data[$gp->parent_id][] = $gp;
		}
		return $data;
    }     
    /*for page manage v.0.1*/

    function get_actparent($id='', $status='')
    {
        $this->db->order_by('set_order','asc');
    	if( ! empty($status)){ $this->db->where('status', $status); $this->db->where('unit_id',0); }
        if( ! empty($id)){ $this->db->where('id',$id); }
        $query = $this->db->get('pages');
        return $query->result();
    }    
    
    function get_go_page($status='', $id='')
    {
    	$go_page = $this->get_actparent($id, $status);
		foreach($go_page as $gp)
		{
			$data[$gp->parent_id][] = $gp;
		}
		return $data;
    }
    
    
}

?>
