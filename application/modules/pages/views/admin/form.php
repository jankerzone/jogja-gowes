<?php if (isset ($id)){echo form_open_multipart(''.PANEL_URL.'/pages/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.PANEL_URL.'/pages/create','class="form form-horizontal"');}?>
    <fieldset>
        
        <div class="control-group">
        <label class="control-label" for="title">Parent Page</label>
        <div class="controls">
            <select name="parent">
                <option value="0">None</option>
                <?php foreach ($parent_menu as $row){?>
                <option <?php if($page?$page->parent_id==$row['id']:'') {echo 'selected=""';}?> value=<?php echo $row['id']?>><?php echo $row['menu']?></option>
                <?php }?>
            </select>
            <?php echo form_error('parent'); ?>
        </div>     
        </div>    
         
        
     
        
        <div class="control-group">
        <label class="control-label" for="set_order">Set Order</label>
        <div class="controls">
            <input type="text" class="input-mini" id="set_order" name="set_order" value="<?php echo set_value('set_order', $page?$page->set_order:''); ?>">
             <?php echo form_error('set_order'); ?>
        </div>     
        </div>        
        
        <div class="control-group">           
        <label class="control-label" for="link">Link To</label>
        <div class="controls">
            <select class="linkto" name="link_to">
                <option value="1"  <?php if($page?$page->link_to=='default':'') {echo 'selected=""';}?>>None</option>
                <option value="2"  <?php if($page?$page->link_to=='url':'') {echo 'selected=""';}?> >URL</option> 
                <?php foreach ($modules as $module){?>
                <option <?php if($page?$page->uri==$module->uri:'') {echo 'selected=""';}?> value="<?php echo $module->uri?>"><?php echo $module->name?></option>
                <?php }?>
            </select>
            <?php echo form_error('parent'); ?>
        </div>     
        </div>          
    <div id="maps" style="display: none">
        <div class="control-group">
        <label class="control-label" for="latitude">Latitude</label>
        <div class="controls">
            <input type="text" class="input-large" id="latitude" name="latitude" value="<?php echo set_value('latitude', $page?$page->latitude:''); ?>">
             <?php echo form_error('latitude'); ?>
        </div>         
        </div>         
         <div class="control-group">
        <label class="control-label" for="longitude">Longitude</label>
        <div class="controls">
            <input type="text" class="input-large" id="longitude" name="longitude" value="<?php echo set_value('longitude', $page?$page->longitude:''); ?>">
             <?php echo form_error('longitude'); ?>
        </div>         
        </div> 
              
         </div>
        <div class="control-group">           
        <label class="control-label" for="link">Open in New Window</label>
        <div class="controls">
            <input type="checkbox"  name="new_window" <?php if($page?$page->new_window==1:'') {echo 'checked=""';}?>  value="1"/>
        </div>     
        </div>        
         
        <div class="control-group">
        <label class="control-label" for="title">Page Title</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="title" name="title" value="<?php echo set_value('title', $page?$page->title:''); ?>">
             <?php echo form_error('title'); ?>
        </div>     
        </div>  
              
        <div class="control-group">
        <label class="control-label" for="title">Page Heading</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="heading" name="heading" value="<?php echo set_value('heading', $page?$page->heading:''); ?>">
             <?php echo form_error('heading'); ?>
        </div>     
        </div> 
        
        <div class="control-group">
        <label class="control-label" for="menu">Menu</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="menu" name="menu" value="<?php echo set_value('menu', $page?$page->menu:''); ?>">
             <?php echo form_error('menu'); ?>
        </div>     
        </div>
                      
        
        <div class="control-group">
        <label class="control-label" for="status">Status</label>
        <div class="controls">
            <select name="status">
                <option value="1"<?php echo set_select('status', '1'); ?> <?php if($page?$page->status=='1':'') {echo 'selected=""';}?> >Publish</option>
                <option value="0"<?php echo set_select('status', '0'); ?><?php if($page?$page->status=='0':'') {echo 'selected=""';}?>>Unpublish</option>
            </select>
             <?php echo form_error('status'); ?>
        </div>     
        </div>    
        <!--- when link to URL-->
        <div id="2" style="display: none;">
        <div class="control-group">
        <label class="control-label" for="url">URL</label>
        <div class="controls">
          <div class="input-prepend"> <span class="add-on">http://</span>
        <input type="text" class="input-xlarge" id="url" name="url" value="<?php echo set_value('url', $page?$page->uri:''); ?>">
        </div> 
        <?php echo form_error('url'); ?>
        </div>     
        </div>  
            <input type="hidden" name="type" value="url"/>
        </div>
         <!--- when link to Default page-->
        <div id="1">
        <div class="control-group">
        <label class="control-label" for="text">Content</label>
        <div class="controls">
            <textarea class="field size1" name="text" id="content_1"><?php echo set_value('text', $page?$page->text:''); ?></textarea>
            <?php echo display_ckeditor($ckeditor_1); ?>  
             <?php echo form_error('text'); ?>
        </div>
       
        </div>
            
        <div class="control-group">
        <label class="control-label" for="text">Attachment</label>
        <div class="controls">
            <input type="file" name="attachment"
        </div>     
        <br/>
        <?php if(!empty ($page->attachment)){?>
        <b>Current Attachment:</b>
        <a href="<?php echo base_url()?>uploads/attachment/<?php echo $page->attachment?>">Download</a>&nbsp;||&nbsp;<?php echo anchor(PANEL_URL.'/pages/delete_attachment/'.$id.'','Delete')?>
        <?php }?>
        </div>
          </div> 
         
         
        <div class="control-group">
        <label class="control-label" for="video_url">Video URL</label>
        <div class="controls">
            <input type="text" class="input-xxlarge" id="meta_desc" name="video_url" value="<?php echo set_value('video_url', $page?$page->video_url:''); ?>">
             <?php echo form_error('video_url'); ?>
            <span class="help-block">Youtube URL</span>
        </div>         
        </div>   
     <div class="control-group">
        <label class="control-label" for="album">Photo Album</label>
        <div class="controls">
        <select name="album">
        <option>None</option>
        <?php foreach ($album as $row){?>
        <option <?php if($page?$page->album_id==$row->id:'') {echo 'selected=""';}?> value=<?php echo $row->id?>><?php echo $row->name_gallery_cat?></option>
        <?php }?>
        </select>
        <?php echo form_error('album'); ?>
        </div>     
      </div>          
         
          </div>
       

          
          <div id="meta">           
                          
         <div class="control-group">
        <label class="control-label" for="meta_kw">Meta Keyword</label>
        <div class="controls">
            <input type="text" class="input-xxlarge" id="meta_kw" name="meta_kw" value="<?php echo set_value('meta_kw', $page?$page->meta_kw:''); ?>">
             <?php echo form_error('meta_kw'); ?>
            <span class="help-block">meta keyword defines keywords for a page, input the keywords separated by comma eg.(clothing, jewelry, nudies)</span>
        </div> 
        
        </div>
               
        <div class="control-group">
        <label class="control-label" for="meta_desc">Meta Description</label>
        <div class="controls">
            <input type="text" class="input-xxlarge" id="meta_desc" name="meta_desc" value="<?php echo set_value('meta_desc', $page?$page->meta_desc:''); ?>">
             <?php echo form_error('meta_desc'); ?>
            <span class="help-block">Two or three words to define a description of a page</span>
        </div>         
        </div>
          <!--    
         <div class="control-group">
        <label class="control-label" for="background">Header</label>
        <div class="controls">
            <input type="file" id="background" name="userfile" >
             <?php echo form_error('background'); ?><br/><br/>
             <?php if($page?$page->background:''){?>
             <b>Current Background Image</b><br/>
             <img src="<?php echo base_url()?>uploads/slides/<?php echo $page->background?>"/>
            <?php }?>
        </div>         
        </div>  -->          
              
         </div>
         
          <div class="form-actions">
            <button type="submit" class="btn btn-primary">Save</button>
            <button class="btn">Cancel</button>
          </div>          
    </fieldset>
        
<?php echo form_close()?>
<script>           
    
    $(".linkto").bind("change", function () {     
      if ($(this).val() == "1") {
            $("#1").slideDown();
            $("#2").slideUp(); 
            $("#meta").show();
        }
        else if($(this).val() =="2") {
            $("#2").slideDown();
            $("#1").slideUp();
            $("#meta").hide();
        }else if($(this).val()=="maps"){
            $("#2").hide();
            $("#1").hide();     
            $("#meta").show();    
            $("#maps").show();  
        }
        else if($(this).val()=="contact"){
            $("#2").hide();
            $("#1").show();     
            $("#meta").show();    
            $("#maps").show();  
        }else if($(this).val()=="welcome"){
            $("#2").hide();
            $("#1").show();     
            $("#meta").show();    
            $("#maps").hide();  
        }
        else{
            $("#2").hide();
            $("#1").hide();     
            $("#meta").show();
        }
        })
        
      $(".unit").bind("change", function () { 
          if ($(this).val() != "0") {
          $("#unit").slideDown();
          }else if($(this).val() == "0"){
          $("#unit").slideUp(); 
          }
      })
      

       <?php if($page){ if($page?$page->welcome_page=='1':'') {
           echo '
               $("#unit").show(); 
            ';
       }} ?>
           
        <?php if($page){ if($page?$page->link_to=='url':'') {
            
            echo '
            $("#2").show();
            $("#1").hide();
            $("#meta").hide();    
            ';
            
            }elseif($page?$page->link_to=='default':''){
                echo'
            $("#1").show();
            $("#2").hide(); 
            $("#meta").show();      
                ';
            }elseif($page?$page->link_to=='maps':''){
                echo ' 
            $("#1").hide();
            $("#2").hide(); 
            $("#meta").show();
            $("#maps").show();';
            }elseif($page?$page->link_to=='contact':''){
            echo '
            $("#1").show();
            $("#2").hide(); 
            $("#meta").show();
            $("#maps").show();        
            ';
            }elseif($page?$page->link_to=='welcome':''){
            echo '
            $("#1").show();
            $("#2").hide(); 
            $("#meta").show();
            $("#maps").hide();        
            ';
            }else{
           echo'
            $("#1").hide();
            $("#2").hide(); 
            $("#meta").show();   
            ';     
            }}
        ?>
</script>
 