<div id="main_view">
<?php echo $msg;?>
<?php echo anchor(''.PANEL_URL.'/pages/create','  <i class="icon-plus icon-white"></i>Add New Page','class="btn btn-primary"')?> &nbsp;&nbsp;<?php echo anchor(''.PANEL_URL.'/pages/do_generate','  <i class="icon-refresh icon-white"></i>Generate Page','class="btn btn-primary"')?><br/><br/>
<?php echo form_open(''.PANEL_URL.'/pages/'); ?>
<input type='hidden' name='suredelete' value='0' />
<ul>
    <li>
        <div class="span4"><input type='checkbox' name='checkall' onclick='CheckAll(this.form);' />&nbsp;<b>Title</b></div><div class="span3"><b>Link To</b></div><div class="span2"><b>Publish</b></div><div class="span2"></div>
    </li>
</ul>
<ul class="sortable">

<?php if($pages) foreach($pages as $page): ?>

<li id="page_<?php echo $page['id']; ?>">
        <div>
            
            <span class="span3"><input type="checkbox" name="checked_<?php echo  $page['id']; ?>" value="<?php echo  $page['id']; ?>" />&nbsp;<a href="#" rel="<?php echo $page['id']; ?>"><?php echo $page['menu']; ?></a></span><span class="span3"> <?php echo $page['link_to']?></span><span class="span3"><?php echo $page['status']?anchor(''.PANEL_URL.'/pages/deactivated/'.$page['id'].'','Yes'):anchor(''.PANEL_URL.'/pages/activated/'.$page['id'].'','No')?></span><span><?php echo anchor(''.PANEL_URL.'/pages/edit/'.$page['id'],'<i class="icon-edit icon-white"></i>Edit','class="btn btn-primary"')?></span>
        </div>

<?php if(isset($page['children'])): ?>
        <ul>
                <?php $controller->tree_builder($page); ?>
        </ul>
</li>

<?php else: ?>

</li>

<?php endif; ?>
<?php endforeach; ?>

</ul>
    <div class="form-actions">
            <button class="btn btn-danger" onclick="ConfirmDelete(this.form, 'Are you sure you want to delete this? It cannot be undone!');">Delete selected</button>
    </div>
 <?php echo form_close()?>

<script type="text/javascript">
       $(document).ready(function(){

        $('.sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            update: function () {
        list = $(this).nestedSortable('toHierarchy', {startDepthCount: 0});
        $.post(
            '<?php echo site_url(''.PANEL_URL.'/pages/order')?>',
            { update_sql: 'ok', order: list }
            
        );
    }
        });

    });
</script>