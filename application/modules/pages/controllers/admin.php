<?php


class Admin extends Admin_Controller{
    
    
  protected $validation_rules = array(
            array(
            'field' => 'menu',
            'label' => 'Menu',
            'rules' => 'required'
            ),      
            array(
            'field' => 'title',
            'label' => 'Title Name',
            'rules' => ''
            ),      
            array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => ''
            ),
            array(
            'field' => 'text',
            'label' => 'Description',
            'rules' => ''
            ),       
            array(
            'field' => 'meta_kw',
            'label' => 'Meta Keyword',
            'rules' => ''
            ), 
            array(
            'field' => 'meta_desc',
            'label' => 'Meta Description',
            'rules' => ''
            ),    
            array(
            'field' => 'heading',
            'label' => 'heading',
            'rules' => ''
            ),   
            array(
            'field' => 'url',
            'label' => 'Url',
            'rules' => ''
            ),     
            array(
            'field' => 'set_order',
            'label' => 'set_order',
            'rules' => ''
            ),         
      
        );


    public function __construct() {
        parent::__construct();
        $this->load->helper('ckeditor');
        $this->load->helper('ajax');
        $this->load->model('gallery/gallery_album');
        $this->load->library('upload');
        $this->form_validation->set_error_delimiters('<div class="alert-error" style="width: 350px;">', '</div>');
        $this->template->append_metadata(theme_js('jquery/jquery.ui.nestedSortable.js')) 
                ->append_metadata(css('index.css','pages')) 
                ->append_metadata(theme_js('forms.js'));
                  
    }
    
    private function upload_config($path){
      $config['upload_path'] = './uploads/'.$path.'';
      $config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|ppt';
      $config['max_size']	= '';
      $config['max_width']  = '';
      $config['max_height']  = '';
      $config['encrypt_name'] = TRUE;
      $this->upload->initialize($config);   
        
    }
    
    protected function parent_menu() {
      return  $this->page_model->get_parent_menu();
    }


    public function index(){
        
                
        $this->data->msg  = '';
        
        if($this->input->post('suredelete') == 1)
        {
                $count_deteted = 0;

                foreach ($this->input->post() as $key=>$val) 
                {
                        if (substr($key, 0, 8) == 'checked_') 
                        {
                                if($this->page_model->delete($val))
                                {
                                        $count_deteted++;
                                }                                                                                             
                        }
                }

                
                if ($count_deteted == 1)
                {
                         $this->data->msg = show_msg('1 row is deleted!', 'success');
                }
                elseif ($count_deteted > 1)
                {
                         $this->data->msg = show_msg($count_deteted . ' rows are deleted!', 'success');
                }
                else
                {
                         $this->data->msg = show_msg('Select at least one row!', 'info');
                }
                $this->generate();
        }
        $this->data->pages = $this->page_model->get_page_tree();
	$this->data->controller =& $this;
        $this->template->title('Pages')              
                ->build('admin/main',$this->data);
    }
    
    
	public function order()
	{
		$order	= $this->input->post('order');
		$data	= $this->input->post('data');
		$root_pages	= isset($data['root_pages']) ? $data['root_pages'] : array();

		if (is_array($order))
		{
			//reset all parent > child relations
			$this->page_model->update_all(array('parent_id' => 0));

			foreach ($order as $i => $page)
			{
				//set the order of the root pages
				$this->page_model->update_by('id', str_replace('page_', '', $page['id']), array('set_order' => $i));

				//iterate through children and set their order and parent
				$this->page_model->_set_children($page);
			}

		
		}
	}

    
        public function tree_builder($page)
        {
            if (isset($page['children'])):

                    foreach($page['children'] as $page): ?>

                            <li id="page_<?php echo $page['id']; ?>">
                                    <div>
                                            <span class="span3"><input type="checkbox" name="checked_<?php echo  $page['id']; ?>" value="<?php echo  $page['id']; ?>" /><a href="#" rel="<?php echo $page['id']; ?>"><?php echo $page['menu']; ?></a></span><span class="span3"> <?php echo $page['link_to']?></span><span class="span3"><?php echo $page['status']?anchor(''.PANEL_URL.'/pages/deactivated/'.$page['id'].'','Yes'):anchor(''.PANEL_URL.'/pages/activated/'.$page['id'].'','No')?></span><span><?php echo anchor(''.PANEL_URL.'/pages/edit/'.$page['id'],'<i class="icon-edit icon-white"></i>Edit','class="btn btn-primary"')?></span>
                                    </div>

                    <?php if(isset($page['children'])): ?>
                                    <ul>
                                                    <?php $this->tree_builder($page); ?>
                                    </ul>
                            </li>
                    <?php else: ?>
                            </li>
                    <?php endif;
                    endforeach;
            endif;
        }    
        
        
    
    public function create(){
    
    //upload config
   
    //Ckeditor's configuration
     $data['ckeditor_1'] = array(
                //ID of the textarea that will be replaced
                'id' 	=> 	'content_1',
                'path'	=>	APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor',
                //Optionnal values
                'config' => array(
                    'toolbar' 	=> 	"Full", 	//Using the Full toolbar
                    'width' 	=> 	"780px",	//Setting a custom width
                    'height' 	=> 	'300px',	//Setting a custom height
                    'filebrowserBrowseUrl'      => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/ckfinder.html',
                    'filebrowserImageBrowseUrl' => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/ckfinder.html?Type=Images',
                    'filebrowserFlashBrowseUrl' => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/ckfinder.html?Type=Flash',
                    'filebrowserUploadUrl'      => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    'filebrowserImageUploadUrl' => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                    'filebrowserFlashUploadUrl' => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'					

                    ),
        );          
     
        
         
         
         if($this->input->post('2')){
            $this->form_validation->set_rules(array_merge($this->validation_rules, array(
            array(
            'field' => 'menu',
            'label' => 'menu',
            'rules' => ''
            ))));
         }else{
             $this->form_validation->set_rules($this->validation_rules);
         }
         

         
         if ($this->form_validation->run() == TRUE){
                

                if($_FILES['attachment']['name']){
                $this->upload_config('attachment');
                      if ($this->upload->do_upload('attachment')){
                      $upload_data = $this->upload->data();
                      $attachment = $upload_data['file_name'];
                      }else{
                      $attachment='';
                      }
                }else{
                     $attachment='';
                }
                

                
             if($this->input->post('link_to')=='2'){
                  $uri = $this->input->post('url');
                  $link_to = 'url';
             }elseif($this->input->post('link_to')=='1'){
                 $uri = $this->fungsi_title->uri_title($this->input->post('title'));
                 $link_to = 'default';
             }else{
                 $uri = $this->fungsi_title->uri_title($this->input->post('link_to'));
                 $link_to = $uri;
             }
            
             $idpage = $this->page_model->insert(
                     array(
                         'uri'=>$uri,
                         'title'=>$this->input->post('title'),
                         'text'=>$this->input->post('text'),
                         'parent_id'=>$this->input->post('parent'),
                         'link_to'=>$link_to,
                         'new_window'=>$this->input->post('new_window'),
                         'menu'=>$this->input->post('menu'),
                         'heading'=>$this->input->post('heading'),
                         'meta_kw'=>$this->input->post('meta_kw'),
                         'meta_desc'=>$this->input->post('meta_desc'),
                         'set_order'=>$this->input->post('set_order'),
                         //'show_on'=>$this->input->post('show_on'),
                         'status'=>$this->input->post('status'),
                         'video_url'=>$this->input->post('video_url'),
                         'album_id'=>$this->input->post('album'),
                         'welcome_page'=>$this->input->post('welcome_page'),
                         'welcome_text'=>$this->input->post('welcome_text'),
                        
                         'longitude'=>$this->input->post('longitude'),
                         'latitude'=>$this->input->post('latitude'),
                         'attachment'=>$attachment?$attachment:''
                     )
                     );
             
             if(!$this->input->post('set_order')){
                 $this->page_model->update($idpage, array('set_order'=>$idpage));
             }
             
             
             $this->generate();
             $this->session->set_flashdata('response', show_msg('The Page was Added successfully!', 'success', '100%'));
             redirect(''.PANEL_URL.'/pages'); 
         }
            
          $data['page']='';
          $data['parent_menu'] = $this->parent_menu();
          $data['modules'] = $this->module_model->get_many_by('in_admin',0);
          $data['album'] = $this->gallery_album->get_all();
          $this->template->title('Create New Page')
                ->build('admin/form',$data);      
    }
    
    public function edit($id=0){
     $id OR redirect(''.PANEL_URL.'/pages');
     
    //upload config
      $this->upload_config('slides');  
      
    //Ckeditor's configuration
     $data['ckeditor_1'] = array(
                //ID of the textarea that will be replaced
                'id' 	=> 	'content_1',
                'path'	=>	APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor',
                //Optionnal values
                'config' => array(
                    'toolbar' 	=> 	"Full", 	//Using the Full toolbar
                    'width' 	=> 	"780px",	//Setting a custom width
                    'height' 	=> 	'300px',	//Setting a custom height
                    'filebrowserBrowseUrl'      => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/ckfinder.html',
                    'filebrowserImageBrowseUrl' => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/ckfinder.html?Type=Images',
                    'filebrowserFlashBrowseUrl' => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/ckfinder.html?Type=Flash',
                    'filebrowserUploadUrl'      => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    'filebrowserImageUploadUrl' => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                    'filebrowserFlashUploadUrl' => APPPATH.'themes/'.$this->settings->admin_theme.'/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'					

                    ),
        );          
     
    
            $this->form_validation->set_rules(array_merge($this->validation_rules, array(
            array(
            'field' => 'menu',
            'label' => 'menu',
            'rules' => 'required'
            ))));

            
            if ($this->form_validation->run() == TRUE){
                
                /*if($_FILES['userfile']['name']){
                $this->upload_config('slides');
                                if ($this->upload->do_upload('userfile')){
                                $upload_data = $this->upload->data();
                                $background = $upload_data['file_name'];
                                $this->page_model->update($id, array('background'=>$background));
                                }
                }*/

                if($_FILES['attachment']['name']){
                $this->upload_config('attachment');
                               if ($this->upload->do_upload('attachment')){
                                $upload_data = $this->upload->data();
                                $background = $upload_data['file_name'];
                                $this->page_model->update($id, array('attachment'=>$background));
                                }
                }
                

 
                
             if($this->input->post('link_to')=='2'){
                  $uri = $this->input->post('url');
                  $link_to = 'url';
             }elseif($this->input->post('link_to')=='1'){
                 $uri = $this->fungsi_title->uri_title($this->input->post('menu'));
                 $link_to = 'default';
             }else{
                 $uri = $this->fungsi_title->uri_title($this->input->post('link_to'));
                 $link_to = $uri;
             }
             $this->page_model->update($id,array(
                         'uri'=>$uri,
                         'title'=>$this->input->post('title'),
                         'text'=>$this->input->post('text'),
                         'parent_id'=>$this->input->post('parent'),
                         'link_to'=>$link_to,
                         'new_window'=>$this->input->post('new_window'),
                         'menu'=>$this->input->post('menu'),
                         'heading'=>$this->input->post('heading'),
                         'meta_kw'=>$this->input->post('meta_kw'),
                         'meta_desc'=>$this->input->post('meta_desc'),
                         'set_order'=>$this->input->post('set_order'),
                         'album_id'=>$this->input->post('album'),
                         'video_url'=>$this->input->post('video_url'),
                         'welcome_page'=>$this->input->post('welcome_page'),
                        'welcome_text'=>$this->input->post('welcome_text'),
                         'longitude'=>$this->input->post('longitude'),
                         'latitude'=>$this->input->post('latitude'),                 
                         //'show_on'=>$this->input->post('show_on'),
                         'status'=>$this->input->post('status'),
       
                     ));
              $this->generate();
             $this->session->set_flashdata('response', show_msg('The Page was Updated successfully!', 'success', '90%'));
          
            redirect(''.PANEL_URL.'/pages'); 
         } 
          $data['page']=$this->page_model->get($id);
          $data['parent_menu'] = $this->parent_menu();
          $data['modules'] = $this->module_model->get_many_by('in_admin',0);
          $data['album'] = $this->gallery_album->get_all();
          $data['id'] = $id;
          $this->template->title('Edit Page')
                ->build('admin/form',$data);      
    }    
    
    public function activated($id){
        $id OR redirect('admin/pages');
        $this->page_model->update($id, array('status'=>1));
        $this->session->set_flashdata('response', show_msg('The Page was Activated successfully!', 'success', '90%'));
         $this->generate();
        redirect(''.PANEL_URL.'/pages'); 
    }
    
    public function deactivated($id){
        $id OR redirect('admin/pages');
        $this->page_model->update($id, array('status'=>0));
        $this->session->set_flashdata('response', show_msg('The Page was Activated successfully!', 'success', '90%'));
         $this->generate();
        redirect(''.PANEL_URL.'/pages'); 
    }    
       
    
    
   public function do_generate(){
       $this->generate();
       $this->session->set_flashdata('response', show_msg('The Page was successfully to Generate !', 'success', '90%'));
       redirect(''.PANEL_URL.'/pages');
   } 
    
   public function generate($id = '1')
    {
                   
        
         
        $menu = '';
        $menu .= '<ul id="sidebarmenu1">';
        
                
        function go_page_tree($data, $parent = 0) 
        {
            
            $CI =& get_instance();
            static $i = 1;
            $link = 'javascript:void()';
            $link_target = '';

        
            $tab = str_repeat("\t\t", $i);
            if (isset($data[$parent])) {
                if($i == 1){$top = ''; }else{ $top = ''; }
                $html = '';
                $i++;
                $ct = 0;
                $ct_p = 0;
                foreach ($data[$parent] as $v) {
                    $child = go_page_tree($data, $v->id);
                    
                    if($v->link_to == 'default')
                    {
                        if(empty ($v->text)){
                          $link = 'javascript:void()';
                        }elseif(!empty ($v->text)){
                            $link = base_url().'page/'.$v->id.'/'.$v->uri.'/';
                        }
                         
                        if($v->new_window == '1')
                        { $link_target = ' target="_blank"'; }else{$link_target='';}
                    }
                    elseif($v->link_to == 'url')
                    {
                        $link = 'http://'.$v->uri;
                        if($v->new_window == '1')
                        { $link_target = ' target="_blank"'; }else{$link_target='';}
                    }
                    elseif($v->link_to == 'maps'){
                       $link = base_url().''.$v->uri.'/index/'.$v->id.'';   
                    }
                    else
                    { $link = base_url().''.$v->uri.'';
                         if($v->new_window == '1')
                        { $link_target = ' target="_blank"'; }else{$link_target='';}
                    }
                    
                    
                    $CI =& get_instance();
                    if($ct == 0 && $ct_p == 0){ $html .= '<li'.$top.'>'; }
                    else{ $html .= '<li>'; }
                    $html .= '<a href="'.$link.'"'.$link_target.'>'.$v->menu.'</a>';
           
                   
                    
                    $html .= "\n";
                    if ($child) {
                        $i--;
                        $html .= '<ul class="child">';
                        $html .= $child;
                        $html .= '</ul>';
                    }
                    
                    
                    
                    $html .= '</li>';
                }
                return $html;
                
            } else {
                return false;
            }
        }
        $data = @$this->page_model->get_go_page(1);
       
        if($data){ $menu .= go_page_tree($data); }
        $menu .= '</ul>';        
                
        file_put_contents('./uploads/mainmenu.html', $menu);
		if($id == '1') {
			//redirect('patrapanel/pages');
		}
    }
    
    public function delete_attachment($page_id=''){
        $this->page_model->update_by('id', $page_id, array('attachment' => ''));
         $this->session->set_flashdata('response', show_msg('The Attachment was deleted successfully!', 'success', '90%'));
        redirect(PANEL_URL.'/pages/edit/'.$page_id.'');
    }
}

?>
