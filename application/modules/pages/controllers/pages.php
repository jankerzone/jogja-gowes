<?php

class Pages extends Public_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('gallery/gallery_model');
    }
   

    public function get($id='',$uri=''){
       
        $data['page'] = $this->page_model->get_page($id);
       $data['attachment'] = $data['page']['attachment'];
        $data['galleries'] = $this->gallery_model->get_many_by(array('id_gallery_cat'=> $data['page']['album_id'],'is_publish'=>'1'));        
        $data['parent_page'] = $this->page_model->get($data['page']['parent_id']?$data['page']['parent_id']:$data['page']['id']);
        $data['sub_page'] = $this->page_model->get_submenu($data['parent_page']->id);
        if(empty ($data['page'])) show_error ('Oppss Sorry the page not founds','404','Oppss'); 
        if($data['page']['status']==0) show_error ('Oppss Sorry the page not founds','404','Oppss');
        $data['background'] = $data['page']['background'];
        
       // print_r($data['backgorund']);
        $this->template->title($this->settings->site_name.' | '.$data['page']['title'])
                ->append_metadata('<meta name="keywords" content="'.$data['page']['meta_kw'].'">')
                ->append_metadata('<meta name="description" content="'.$data['page']['meta_desc'].'">')     
                ->append_metadata(
                        '<script>
                           
                            
                            $(function() {
                               $("a[rel^=lightbox]").lightBox({ imageLoading: "'.base_url().APPPATH.'/themes/echo/img/lightbox-ico-loading.gif", imageBtnClose: "'.base_url().APPPATH.'/themes/echo/img/lightbox-btn-close.gif", imageBtnPrev: "'.base_url().APPPATH.'/themes/echo/img/lightbox-btn-prev.gif", imageBtnNext: "'.base_url().APPPATH.'/themes/echo/img/lightbox-btn-next.gif",});
                            });

                           
                            </script>'
                        )
                ->append_metadata(theme_js('jquery.lightbox-0.5.min.js'))
                ->append_metadata(theme_css('jquery.lightbox-0.5.css'))
                ->build('detail_page',$data);
    }
 
}

?>


