<?php if (isset ($id)){echo form_open_multipart(''.PANEL_URL.'/jalurgowes/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.PANEL_URL.'/jalurgowes/create','class="form form-horizontal"');}?>
    <fieldset>
    
        <div class="control-group">
        <label class="control-label" for="jalur">Nama Jalur</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="jalur" name="jalur" value="<?php echo set_value('jalur', $jalur?$jalur->jalur:''); ?>">
             <?php echo form_error('jalur'); ?>
        </div>     
        </div>
        
        <div class="control-group">
        <p>Paste link dari langkah berikut : <br>
        1) Buat jalur dengan bantuan Google Earth<br/>
        2) Export ke dalam bentuk file .KML <br/>
        3) Convert ke <a href="www.gspies.com">gspies.com</a> agar jalur terdeteksi di GPS <br/>
        4) Upload file KML ke filehosting dan paste ke <a href="http://www.nearby.org.uk/google/fake-kmlgadget.php">KML generator</a> <br/>
        5) Tempel link URL ke kolom di bawah ini</p>
        <label class="control-label" for="link">Link</label>
        <div class="controls"><textarea id="link" style="height:200px; width:70%;" name="link"><?php echo set_value('link', $jalur?$jalur->link:''); ?></textarea>
             <?php echo form_error('link'); ?>
        </div>     
        </div>        
        

        <div class="control-group">
        <label class="control-label" for="deskripsi">Deskripsi</label>
        <div class="controls"><textarea id="deskripsi" style="height:200px; width:70%;" name="deskripsi"><?php echo set_value('deskripsi', $jalur?$jalur->deskripsi:''); ?></textarea>
             <?php echo form_error('link'); ?>
        </div>     
        </div>
        
        <div class="form-actions">
            <button class="btn btn-success" value="submit">Simpan</button>
        </div>
        
    </fieldset>

<?php echo form_close()?>

