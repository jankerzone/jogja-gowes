<h3>Fitur Pencarian Jalur Gowes</h3>
<!-- content area -->    
	<section id="content">
    	<div class="clearfix">
	    	<?php foreach($jalurgowes as $row){ ?>
	    	<div class="grid_12">
	    	<h2><?php echo $row->jalur ;?></h2>
	    	<p><?php echo word_limiter($row->deskripsi, 25) ;?> <br/><?php echo anchor(BASE_URL.'jalurgowes/detail/'.$row->id.'/'. url_title($row->jalur),'(Lihat Jalur)') ;?></p>
	    	</div>
	    	<?php } ?>
	    	<div class="grid_12">
	    		<center><?php echo $this->pagination->create_links();?></center>
	    	</div>
		</div>                    
	</section>
