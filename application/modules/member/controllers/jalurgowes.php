<?php

class Jalurgowes extends Base_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('jalurgowes_model');

        $config['base_url'] = base_url('jalurgowes/hal/');
        $config['total_rows'] = 100;
        $config['per_page'] = 5; 
        $this->pagination->initialize($config); 

    }
    
    public function index()
    {
    	$data['tagline']	= 'Jalur Gowes';
        $this->jalurgowes_model->limit(5,0);
        $data['jalurgowes']		= $this->jalurgowes_model->get_all();

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('Jalur Gowes :: Portal Komunitas Jogja Gowes')
					   ->build('jalurgowes', $data);
    }

    public function hal($id = '')
    {
        $id = $this->uri->segment(3);

        $data['tagline']    = 'News Halaman '.$id;

        $this->news_model->limit(5,$id);
        $data['news']       = $this->news_model->get_all();
        
        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('News :: Portal Komunitas Jogja Gowes')
                       ->build('news', $data);
    }

    public function detail($id)
    {
        $data['id'] = $id;
        
        $data['tagline']      = 'Jalur Gowes';
        $data['jalur']   = $this->jalurgowes_model->get($id);

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('Jalur Gowes :: Portal Komunitas Jogja Gowes')
                       ->build('detail_jalurgowes', $data);       
    }
}

?>
