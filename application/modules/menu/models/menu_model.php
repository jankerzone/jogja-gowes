<?php

class Menu_model extends MY_Model{
    
    public function __construct() {
        parent::__construct();
    }
    

    public function get_parent_menu($param=FALSE){
        $this->db->order_by('set_order','asc');
        $this->db->where('status',1);
        $param;
        $q = $this->db->get('menus');
        return $q->result_array();
    }
    
}

?>
