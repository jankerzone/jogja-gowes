<?php echo $msg;?>
<?php echo anchor(''.PANEL_URL.'/menu/create','  <i class="icon-plus icon-white"></i>Add New Menu','class="btn btn-primary"')?><br/><br/>

        <?php echo form_open(''.PANEL_URL.'/menu/'); ?>
<input type='hidden' name='suredelete' value='0' />
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th width="5%"><center><input type='checkbox' name='checkall' onclick='CheckAll(this.form);' /></center></th>
            <th>Title</th>
            <th>Link Type</th>
            <th>Status</th>
            <th>Set Order</th>
            <th></th>
        </tr>
    </thead>
  
    <tbody>
        <?php foreach($menus as $menu){?>
        <tr>
            <td>
        <center>
                <input type="checkbox" name="checked_<?php echo  $menu->id; ?>" value="<?php echo  $menu->id; ?>" />
        </center>                
            </td>
            <td>
                <?php echo $menu->title?>
            </td>
            <td>
                <?php echo $menu->link_type?>
            </td>
            <td>
                <?php echo $menu->status?anchor(''.PANEL_URL.'/menu/unpublish/'.$menu->id.'','Publish'):anchor(''.PANEL_URL.'/menu/publish/'.$menu->id.'','Unpublish')?>
            </td>
            <td>
                <?php echo $menu->set_order ?>
            </td>
            <td class="span2">
                <?php echo anchor(''.PANEL_URL.'/menu/edit/'.$menu->id,'<i class="icon-edit icon-white"></i>Edit','class="btn btn-primary"')?>
            </td>

        </tr>
        <?php }?>
        
    </tbody>
    
</table>
    <div class="form-actions">
            <button class="btn btn-danger" onclick="ConfirmDelete(this.form, 'Are you sure you want to delete this? It cannot be undone!');">Delete selected</button>
    </div>
<?php echo form_close()?>


