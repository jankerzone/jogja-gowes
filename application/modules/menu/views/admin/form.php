<?php if (isset ($id)){echo form_open_multipart(''.PANEL_URL.'/menu/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.PANEL_URL.'/menu/create','class="form form-horizontal"');}?>
<fieldset>
    
<div class="control-group">
<label class="control-label" for="title">Title</label>
<div class="controls">
<input type="text" class="input-xlarge" id="title" name="title" value="<?php echo set_value('title', $menu?$menu->title:''); ?>">
 <?php echo form_error('title'); ?>
</div>     
</div>
<div class="control-group">
<label class="control-label" for="set_order">Set Order</label>
<div class="controls">
<input type="text" class="input-mini" id="set_order" name="set_order" value="<?php echo set_value('set_order', $menu?$menu->set_order:''); ?>">
 <?php echo form_error('set_order'); ?>
</div>     
</div>    
<!--
<div class="control-group">
<label class="control-label" for="parent">Parent Menu</label>
<div class="controls">
    <select name="parent">
        
        <option value="0">None</option>
        <?php foreach ($parent_menu as $row){?>
        <option <?php if($menu?$menu->parent_id==$row['id']:'') {echo 'selected=""';}?> value=<?php echo $row['id']?>><?php echo $row['title']?></option>
        <?php }?>
    </select>
    <?php echo form_error('parent'); ?>
</div>     
</div>     
-->
<div class="control-group">
<label class="control-label" for="status">Status</label>
<div class="controls">
    <select name="status">
        <option value="1"<?php echo set_select('status', '1'); ?> <?php if($menu?$menu->status=='1':'') {echo 'selected=""';}?> >Publish</option>
        <option value="0"<?php echo set_select('status', '0'); ?><?php if($menu?$menu->status=='0':'') {echo 'selected=""';}?>>Unpublish</option>
    </select>
        <?php echo form_error('status'); ?>
</div>     
</div> 
<div class="control-group">
<label class="control-label" for="link_type">Link Type</label>
<div class="controls">
    <label class="radio">
        <input type="radio" name="link_type" value="url_type" <?php  if($menu?$menu->link_type=='url':'') {echo 'checked=""';}?> onclick="showTo(this.value)"/> URL
    </label>
    <label class="radio">
    <input type="radio" name="link_type" value="page_type" <?php  if($menu?$menu->link_type=='page':'') {echo 'checked=""';}?> onclick="showTo(this.value)"/> Page
    </label>
     <label class="radio">
    <input type="radio" name="link_type" value="module_type" <?php  if($menu?$menu->link_type=='module':'') {echo 'checked=""';}?> onclick="showTo(this.value)"/> Module
     </label>
 <?php echo form_error('link_type'); ?>
</div>     
</div>
<div id="url_type" style="display: none;">
 <div class="control-group">
<label class="control-label" for="url">URL</label>
<div class="controls">
<input type="text" class="input-xlarge" id="title" name="url" value="<?php echo set_value('url', $menu?$menu->url:'http://'); ?>">
</div>     
</div>   
</div>
    
<div id="page_type" style="display: none;">
 <div class="control-group">
<label class="control-label" for="page">Page</label>
<div class="controls">
    <select name="page">
        <?php foreach($pages as $page){?>
        <option <?php  if($menu?$menu->page_id==$page->id:'') {echo 'selected=""';}?> value="<?php echo $page->id?>">
            <?php echo $page->menu?>
        </option>
        <?php }?>
    </select>   
</div>     
</div>   
</div>    
    
<div id="module_type" style="display: none;">
 <div class="control-group">
<label class="control-label" for="module">Module</label>
<div class="controls">
    <select name="module">
        <option value="0">None</option>
        <?php foreach ($modules as $module){?>
        <option <?php  if($menu?$menu->module_name==$module->uri:'') {echo 'selected=""';}?> value=<?php echo $module->uri?>><?php echo $module->name?></option>
        <?php }?>
    </select>
    <?php echo form_error('parent'); ?>
</div>    
</div>   
</div>      
    
<div class="form-actions">
<button type="submit" class="btn btn-primary">Save</button>
<button class="btn">Cancel</button>
</div>      
    
</fieldset>

<script>
    showTo('<?php echo $menu?$menu->link_type:''?>_type');
</script>