<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin extends Admin_Controller{
    
    protected $validation_rules = array(
        array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'required'
        ),
        array(
            'field' => 'link_type',
            'label' => 'Link Type',
            'rules' => 'required'
        )
        );
    
    public function __construct() {
        parent::__construct();
        $this->load->model('modules/module_model');
        $this->form_validation->set_error_delimiters('<div class="alert-error" style="width: 350px;">', '</div>');
        $this->template->append_metadata(theme_js('forms.js'));
    }

    
    protected function parrent_menu(){
        return  $this->menu_model->get_parent_menu($this->db->where('parent_id =',0));
    }


    public function index(){
              $data['msg'] = '';
        
        if($this->input->post('suredelete') == 1)
        {
                $count_deteted = 0;

                foreach ($this->input->post() as $key=>$val) 
                {
                        if (substr($key, 0, 8) == 'checked_') 
                        {
                                if($this->menu_model->delete($val))
                                {
                                        $count_deteted++;
                                }                                                                                             
                        }
                }


                if ($count_deteted == 1)
                {
                        $data['msg'] = show_msg('1 row is deleted!', 'success');
                }
                elseif ($count_deteted > 1)
                {
                        $data['msg'] = show_msg($count_deteted . ' rows are deleted!', 'success');
                }
                else
                {
                        $data['msg'] = show_msg('Select at least one row!', 'info');
                }
        }
        
        $data['menus'] = $this->menu_model->get_all();
        $this->template->title('Menu management')              
                ->build('admin/main',$data);
    }
    
    public function create(){
        
          $this->form_validation->set_rules($this->validation_rules);
          if ($this->form_validation->run() == TRUE){
              
             $id =  $this->menu_model->insert(
                    array(
                        'title'=>$this->input->post('title'),
                        //'parent_id'=>$this->input->post('parent'),
                        'set_order'=>$this->input->post('set_order'),
                        'status'=>$this->input->post('status')
                        )
                      );
             if($this->input->post('link_type')=='url_type'){
                 $this->menu_model->update($id,array('url'=>$this->input->post('url'),'link_type'=>'url'));
             }elseif($this->input->post('link_type')=='page_type'){
                 $this->menu_model->update($id,array('page_id'=>$this->input->post('page'),'link_type'=>'page'));
             }elseif($this->input->post('link_type')=='module_type'){
                 $this->menu_model->update($id,array('module_name'=>$this->input->post('module'),'link_type'=>'module'));
             }
             $this->session->set_flashdata('response', show_msg('The Menu was Added successfully!', 'success', '100%'));
             redirect(''.PANEL_URL.'/menu');
              
          }
          $data['menu']='';
          $data['parent_menu'] = $this->parrent_menu();
          $data['pages'] = $this->page_model->get_all();
          $data['modules'] = $this->module_model->get_many_by('in_admin',0);
          $this->template->title('Create New Menu')
                ->build('admin/form',$data); 
    }
    
    public function edit($id=0){
        $id OR redirect('admin/menu');  
        $data['id']=$id;
        $data['menu']= $this->menu_model->get($id);
        if(empty ($data['menu'])): redirect('admin/menu'); endif;
        $this->form_validation->set_rules($this->validation_rules);
          if ($this->form_validation->run() == TRUE){
              
             $this->menu_model->update($id,
                    array(
                        'title'=>$this->input->post('title'),
                        //'parent_id'=>$this->input->post('parent'),
                        'set_order'=>$this->input->post('set_order'),
                        'status'=>$this->input->post('status')
                        )
                      );
             if($this->input->post('link_type')=='url_type'){
                 $this->menu_model->update($id,array('url'=>$this->input->post('url'),'link_type'=>'url'));
             }elseif($this->input->post('link_type')=='page_type'){
                 $this->menu_model->update($id,array('page_id'=>$this->input->post('page'),'link_type'=>'page'));
             }elseif($this->input->post('link_type')=='module_type'){
                 $this->menu_model->update($id,array('module_name'=>$this->input->post('module'),'link_type'=>'module'));
             }
             $this->session->set_flashdata('response', show_msg('The Menu was Updated successfully!', 'success', '100%'));
              redirect(''.PANEL_URL.'/menu');   
          }
          
          $data['parent_menu'] = $this->parrent_menu();
          $data['pages'] = $this->page_model->get_all();
          $data['modules'] = $this->module_model->get_many_by('in_admin',0);
          $this->template->title('Edit Menu')
                ->build('admin/form',$data); 

    }
    
    public function unpublish($id=''){
        $this->menu_model->update($id, array('status'=>0));
        redirect(''.PANEL_URL.'/menu');
    }
    public function publish($id=''){
        $this->menu_model->update($id, array('status'=>1));
        redirect(''.PANEL_URL.'/menu');
    }
    
}

?>
