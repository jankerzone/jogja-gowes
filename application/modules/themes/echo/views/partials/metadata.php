
<?php echo theme_css('style.css')?>
<?php echo theme_css('jquery.scrollbarpaper.css')?>
<?php echo theme_js('jquery/jquery.min.js'); ?>
<?php echo theme_js('jquery/jqueryui.js'); ?>
<?php echo theme_js('jquery.backstretch.min.js'); ?>
<?php echo theme_js('jquery.scrollbarpaper.js'); ?>
<?php //echo theme_js('jquery.easing.js'); ?>
<?php //echo theme_js('jquery.mousewheel.js'); ?>
<?php echo theme_js('header.js')?>
<?php echo $template['metadata'];?>


<script type="text/javascript">
$(function() {
  $('#outer_container').scrollbarPaper();
});
</script>
