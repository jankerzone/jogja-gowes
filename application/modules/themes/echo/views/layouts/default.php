<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $template['title'];?></title>
         <?php echo $template['partials']['metadata']; ?> 
        <!-- Full background-->
        <script>
            
            // Create an array of images that you'd like to use
            var images = [
               '<?php echo base_url()?>uploads/slides/<?php echo $this->settings->bg_image?>',
            ];
            // A little script for preloading all of the images
            // It's not necessary, but generally a good idea
            $(images).each(function(){
            $('<img/>')[0].src = this; 
            });
            // The index variable will keep track of which image is currently showing
            var index = 0;
            // Call backstretch for the first time,
            // In this case, I'm settings speed of 500ms for a fadeIn effect between images.
            $.backstretch(images[index], {speed: 500});
            // Set an interval that increments the index and sets the new image
            // Note: The fadeIn speed set above will be inherited
            setInterval(function() {
                index = (index >= images.length - 1) ? 0 : index + 1;
                $.backstretch(images[index]);
            }, 5000);
                     
        </script>      
        <!-- End of Full background-->        
    </head>
    <body>
        <div class="wrapper">
        <div class="header">
            <div class="logo">
                <a href="<?php echo site_url()?>"><?php echo theme_image('logo.png')?></a>
            </div>
        </div>
        
        <div class="left_content">
            <div class="sidebarmenu">
    <?php
    @include FCPATH.'uploads/mainmenu.html'; 
    ?></div>
            
        </div>
        
        <div class="right_content">
           <?php if($this->router->class != 'welcome'){?>
         <div id="outer_container">  
             <div class="content">
        <?php echo $template['body']?>
             </div>
        </div>
            <div class="clear"></div>
            <div class="sharing_tools">
              	<div style="float: right; margin-right: 10px; " > 
                	<?php echo $this->settings->sharing_tools;?>
		</div>
            </div>
            <div class="copy">
             &copy; Copyright <?php echo date("Y",time()); ?> &nbsp <strong><a href="http://<?php echo $_SERVER['SERVER_NAME']; ?>"><?php echo $this->settings->site_name ?></strong> powered by <a href="http://www.interaksi.co.id" target="_blank">Interaksi</a>
             </div>

            <?php }elseif($this->router->class == 'welcome'){ if($page){?>
         <div id="outer_container">  
             <div class="content">
        <?php echo $template['body']?>
             </div>
        </div>
            <div class="clear"></div>
            <div class="sharing_tools">
		<div style="float: right; margin-right: 10px;" >                
                	<?php echo $this->settings->sharing_tools;?>
		</div>
            </div>            
	    <div class="copy">
             &copy; Copyright <?php echo date("Y",time()); ?> &nbsp <strong><a href="http://<?php echo $_SERVER['SERVER_NAME']; ?>"><?php echo $this->settings->site_name ?></strong> powered by <a href="http://www.interaksi.co.id" target="_blank">Interaksi</a>


             </div>

            <?php } }?>
        </div>
        </div>
        <div class="clear"></div>
        
    </body>
</html>
