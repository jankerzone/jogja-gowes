<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $template['title']; ?></title>
		<?php echo $template['partials']['metadata']; ?>
	</head>
	<body>
            <div class="container">
		<legend><?php echo $template['title']; ?></legend>
                <?php echo $template['body']; ?>
                <?php echo $template['partials']['footer']; ?>
                </div>
	</body>
        
</html>