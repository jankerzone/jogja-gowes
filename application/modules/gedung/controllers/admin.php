<?php

class Admin extends Admin_Controller{
    
    
    public function __construct() {
        parent::__construct();
        $this->load->model('gedung_model');
    }
    
    public function index(){
        $data['gedungs']= $this->gedung_model->get_all();
        $this->template->title('Gedung Management')
                ->append_metadata(theme_js('forms.js'))
                ->build('admin/main',$data);
    }
    
    public function create(){
        
        $this->form_validation->set_rules('nama_gedung', 'Nama Gedung', 'required');
        $this->form_validation->set_rules('quota', 'Quota', 'required|numeric');
        $this->form_validation->set_rules('thema', 'Thema', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        
        if ($this->form_validation->run() == TRUE)
        {
                $this->gedung_model->insert(array('nama_gedung'=>$this->input->post('nama_gedung'),
                        'quota'=>$this->input->post('quota'),
                        'thema'=>$this->input->post('thema'),
                        'harga'=>$this->input->post('harga'),
                        ));
                
                $this->session->set_flashdata(show_msg('Gedung berhasil disimpan', 'success'));
                redirect(PANEL_URL.'/gedung');
        }
        
        $data['gedung'] = '';
        $this->template->title('Tambah Gedung')
                ->build('admin/form',$data);      
    }
    
    
}


?>
