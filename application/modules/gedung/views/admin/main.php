<?php echo anchor(PANEL_URL.'/gedung/create','Tambah Gedung','class="btn btn-primary"')?><br/><br/>
<table class="table table-striped table-bordered">
        <thead>
		<tr>
                    <th width="5%"><center><input type='checkbox' name='checkall' onclick='CheckAll(this.form);' /></center></th>
			<th>Nama Gedung</th>
			<th>Quota</th>
			<th>Thema</th>
                        <th>Harga</th>
                        <th>Aksi</th>
		</tr>
        </thead>
        <tbody>
            <?php foreach($gedungs as $gedung){?>
            <tr>
                <td><center><input type="checkbox" name="checked_<?php echo  $gedung->id; ?>" value="<?php echo  $gedung->id; ?>" /></center></td>
                <td><?php echo $gedung->nama_gedung?></td>
                <td><?php echo $gedung->quota?></td>
                <td><?php echo $gedung->thema?></td>
                <td><?php echo $gedung->harga?></td>
                <td></td>
            </tr>
            <?php }?>
        </tbody>
        
</table>       