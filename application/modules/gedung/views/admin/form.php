<?php if (isset ($id)){echo form_open_multipart(''.PANEL_URL.'/gedung/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.PANEL_URL.'/gedung/create','class="form form-horizontal"');}?>
    <fieldset>
        
        <div class="control-group">
        <label class="control-label" for="nama_gedung">Nama Gedung</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="nama_gedung" name="nama_gedung" value="<?php echo set_value('nama_gedung', $gedung?$gedung->nama_gedung:''); ?>">
             <?php echo form_error('nama_gedung'); ?>
        </div>     
        </div>
        
        <div class="control-group">
        <label class="control-label" for="quota">Quota</label>
        <div class="controls">
            <input type="text" class="input-mini" id="quota" name="quota" value="<?php echo set_value('quota', $gedung?$gedung->quota:''); ?>">
             <?php echo form_error('quota'); ?>
        </div>     
        </div>        
        
        <div class="control-group">
        <label class="control-label" for="thema">Thema</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="quota" name="thema" value="<?php echo set_value('thema', $gedung?$gedung->thema:''); ?>">
             <?php echo form_error('thema'); ?>
        </div>     
        </div>   
        
        <div class="control-group">
        <label class="control-label" for="harga">Harga</label>
        <div class="controls">
            <input type="text" class="input-small" id="harga" name="harga" value="<?php echo set_value('harga', $gedung?$gedung->harga:''); ?>">
             <?php echo form_error('harga'); ?>
        </div>     
        </div>         
        
        <div class="form-actions">
            <button class="btn btn-success">Simpan</button>
        </div>
        
    </fieldset>

<?php echo form_close()?>