<?php echo anchor(PANEL_URL.'/notifikasi/edit/1','Edit notifikasi','class="btn btn-primary"')?><br/><br/>
<table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Judul</th>
            <th>Isi</th>
            <th>Publish</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($notifikasis as $notifikasi){?>
            <tr>
                <td><?php echo $notifikasi->judul?></td>
                <td><?php echo $notifikasi->konten?></td>
                <td width="100px"><?php echo $notifikasi->publish?></td>
            </tr>
            <?php }?>
        </tbody>
        
</table>       