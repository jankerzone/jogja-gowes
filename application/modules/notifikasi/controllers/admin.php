<?php

class Admin extends Admin_Controller{
    
    
    public function __construct() {
        parent::__construct();
        $this->load->model('notifikasi_model');

    }
    
    public function index(){
        $data['notifikasis']= $this->notifikasi_model->get_all();

        $data['notifikasi'] = '';
        $this->template->title('Manajemen Notifikasi')
                ->append_metadata(theme_js('forms.js'))
                ->build('admin/main',$data);
    }
    
    
    
    public function edit(){
        $data['id']         = $this->uri->segment(4);
        $id                 = $data['id'];

        $this->form_validation->set_rules('judul', 'Judul Notifikasi', 'required');
        $this->form_validation->set_rules('konten', 'Konten', 'required');

        if($this->form_validation->run() == TRUE){
            $this->notifikasi_model->update($id,array(
                    'judul'  => $this->input->post('judul'),
                    'konten' => $this->input->post('konten'),
                    'publish'=> $this->input->post('publish')
                )
            );

            redirect('gowespanel/notifikasi'); 
        }

        $data['notifikasis'] = $this->notifikasi_model->get_all();
        $this->template->title('Edit Notifikasi')
                ->build('admin/form',$data);  



        $this->template->title('Edit Notifikasi')
                ->build('admin/form',$data); 
    }

   
}


?>
