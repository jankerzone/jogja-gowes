<span class="clearfix"><?php echo anchor(base_url(),'Home') ;?> > <?php echo anchor('profil','Daftar Profil') ;?> > <?php echo anchor(current_url(),$profil->nama) ;?></span>
<!-- content area -->    
	<section id="content">
    	<div class="clearfix">

	    	<div class="grid_12">
	    	<h1><?php echo $profil->nama ?></h1>
	    	<span><center><img src="<?php echo base_url('assets/upload/'.$profil->picture) ?>" width="300px"></center>
	    	<p><?php echo nl2br($profil->deskripsi) ?></p><hr/>
	    	<h3>List Pengurus</h3>
	    	<p><?php echo nl2br($profil->pengurus) ?></p><hr/>
	    	<h3>Social Media</h3>
	    	<span><a href="https://twitter.com/<?php echo $profil->socmed_2 ?>" target="_blank" /><img src="<?php echo base_url('assets/images/iconmonstr-twitter-5-icon.svg') ?>" width="48px"/></a> &nbsp; <a href="https://facebook.com/<?php echo $profil->socmed_1 ?>" target="_blank" /><img src="<?php echo base_url('assets/images/iconmonstr-facebook-5-icon.svg') ?>" width="48px"/></a></span>
                
	    	</div>                  
	</section>
