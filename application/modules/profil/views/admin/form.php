<?php if (isset ($id)){echo form_open_multipart(''.PANEL_URL.'/profil/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.PANEL_URL.'/profil/create','class="form form-horizontal"');}?>
    <fieldset>
    
        <div class="control-group">
        <label class="control-label" for="nama">Nama Profil Komunitas</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="nama" name="nama" value="<?php echo set_value('nama', $profil?$profil->nama:''); ?>">
             <?php echo form_error('nama'); ?>
        </div>     
        </div>
        
        <div class="control-group">
        <label class="control-label" for="deskripsi">Deskripsi Profil</label>
        <div class="controls"><textarea id="deskripsi" style="height:200px; width:70%;" name="deskripsi"><?php echo set_value('deskripsi', $profil?$profil->deskripsi:''); ?></textarea>
             <?php echo form_error('deskripsi'); ?>
        </div>     
        </div>        
        
        <div class="control-group">
        <label class="control-label" for="pengurus">List Pengurus</label>
        <div class="controls"><textarea id="pengurus" style="height:200px; width:70%;" name="pengurus"><?php echo set_value('pengurus', $profil?$profil->pengurus:''); ?></textarea>
             <?php echo form_error('pengurus'); ?>
        </div>     
        </div>  

        <div class="control-group">
        <label class="control-label" for="socmed_1">Akun Facebook (Username) </label>
        <div class="controls">
            <input type="text" class="input-xmedium" id="socmed_1" name="socmed_1" value="<?php echo set_value('socmed_1', $profil?$profil->socmed_1:''); ?>">
        </div>     
        </div>

        <div class="control-group">
        <label class="control-label" for="socmed_1">Akun Twitter (@Username) </label>
        <div class="controls">
            <input type="text" class="input-xmedium" id="socmed_2" name="socmed_2" value="<?php echo set_value('socmed_2', $profil?$profil->socmed_2:''); ?>">
        </div>     
        </div>                

        <?php if ( isset ($profil->picture))
            {
                $url = base_url('assets/upload/'.$profil->picture);
            } 
            else
            {
                $url = base_url('assets/upload/noimg.gif');
            } 
            ?>
        <div class="control-group">
        <label class="control-label" for="picture">Picture</label>
        <div class="controls">
        <img src="<?php echo $url ?>" title="preview" width="250px"> <br/>
           <input type="file" name="userfile"/>                
         <?php echo form_error('picture'); ?>
        </div>     
        </div> 
        
        <div class="form-actions">
            <button class="btn btn-success" value="submit">Simpan</button>
        </div>
        
    </fieldset>

<?php echo form_close()?>

