<?php echo anchor(PANEL_URL.'/profil/create','Tambah Profil','class="btn btn-primary"')?><br/><br/>
<table class="table table-striped table-bordered">
        <thead>
		<tr>
            <th>Nama Profil Komunitas</th>
			<th>Deskripsi</th>
			<th>List Pengurus</th>
            <th>Aksi</th>
		</tr>
        </thead>
        <tbody>
            <?php foreach($profil as $row){?>
            <tr>
                <td><b><?php echo $row->nama?></b></td>
                <td><?php if($row->picture != NULL)
                {
                    echo '<center><img src="'.base_url('assets/upload/'.$row->picture).'" width="350px"  /></center>'; 
                } else {
                    echo '<img src="'.base_url('assets/upload/noimg.gif').'" />';
                }?>
                <br/> <p><?php echo nl2br($row->deskripsi)?></p> 
                <span><a href="https://twitter.com/<?php echo $row->socmed_2 ?>" target="_blank" /><img src="<?php echo base_url('assets/images/iconmonstr-twitter-5-icon.svg') ?>" width="48px"/></a> &nbsp; <a href="https://facebook.com/<?php echo $row->socmed_1 ?>" target="_blank" /><img src="<?php echo base_url('assets/images/iconmonstr-facebook-5-icon.svg') ?>" width="48px"/></a></span>
                </td>
                <td width="100px"><?php echo nl2br($row->pengurus) ?></td>
                <td width="120px"><?php echo anchor(PANEL_URL.'/profil/edit/'.$row->id,'Edit','class="btn btn-warning"')?> 
                <?php echo anchor(PANEL_URL.'/profil/delete/'.$row->id,'Hapus','class="btn btn-danger"')?></td>
            </tr>
            <?php }?>
        </tbody>
        
</table>       