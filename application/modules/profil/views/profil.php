<h3>Daftar Profil Komunitas Gowes di Jogja</h3>
<!-- content area -->    
	<section id="content">
    	<div class="clearfix">
	    	<?php foreach($profil as $row){ ?>
	    	<div class="grid_3">
	    	<?php if($row->picture != NULL)
                {
                    echo '<img src="'.base_url('assets/upload/'.$row->picture).'" width="350px" title="'.$row->nama.'"  />'; 
                } else {
                    echo '<img src="'.base_url('assets/upload/noimg.gif').'" />';
                }?>
	    	</div>
	    	<div class="grid_9">
	    	<h2><?php echo $row->nama ;?></h2>
	    	<p><?php echo word_limiter($row->deskripsi, 25) ;?> <br/><?php echo anchor(BASE_URL.'profil/detail/'.$row->id.'/'. url_title($row->nama),'(baca profil selengkapnya)') ;?></p>
	    	</div>
	    	<?php } ?>
	    	<div class="grid_12">
	    		<center><?php echo $this->pagination->create_links();?></center>
	    	</div>
		</div>                    
	</section>
