<?php

class Profil extends Base_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('profil_model');

        $config['base_url'] = base_url('profil/hal/');
        $config['total_rows'] = 100;
        $config['per_page'] = 5; 
        $this->pagination->initialize($config); 

    }
    
    public function index()
    {
    	$data['tagline']	= 'Profil';

        $this->profil_model->limit(5,0);
        $data['profil']		= $this->profil_model->get_all();

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();
        $this->template->title('Profil :: Portal Komunitas Jogja Gowes')
					   ->build('profil', $data);
    }

    public function hal($id = '')
    {
        $id = $this->uri->segment(3);

        $data['tagline']    = 'Profil Halaman '.$id;

        $this->profil_model->limit(5,$id);
        $data['profil']       = $this->profil_model->get_all();
        
        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar'] = $this->sidebar->ambil_profil();
        $this->template->title('Profil :: Portal Komunitas Jogja Gowes')
                       ->build('profil', $data);
    }

    public function detail($id)
    {
        $data['id'] = $id;
        
        $data['tagline']    = 'Profil';
        $data['profil']       = $this->profil_model->get($id);

        $this->profil_model->order_by('id', 'DESC');
        $this->profil_model->limit(5,0);

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar'] = $this->sidebar->ambil_profil();
        $this->template->title('Profil :: Portal Komunitas Jogja Gowes')
                       ->build('detail_profil', $data);       
    }
}

?>
