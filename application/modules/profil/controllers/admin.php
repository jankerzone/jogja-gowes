<?php

class Admin extends Admin_Controller{
    
    
    public function __construct() {
        parent::__construct();
        $this->load->model('profil_model');
    }
    
    private function upload_config()
    {
      $config['upload_path'] = './assets/upload/';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['max_size']   = '2048'; //satuan kilobytes
      $config['max_width']  = '';
      $config['max_height']  = '';
      $config['encrypt_name'] = TRUE;
      $this->upload->initialize($config);
    }

    public function index()
    {
        $this->profil_model->order_by('id',$order = 'DESC');
        $data['profil']  = $this->profil_model->get_all();

        $this->template->title('Profil Management')
                ->append_metadata(theme_js('forms.js'))
                ->build('admin/main',$data);
    }

    public function create()
    {
        $this->upload_config();
        $this->form_validation->set_rules('nama', 'Nama Profil Komunitas', 'required|xss_clean');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|xss_clean');
        $this->form_validation->set_rules('pengurus', 'List Pengurus', 'required|xss_clean');

        if($this->form_validation->run() == TRUE )
        {
            $unggah = $this->profil_model->insert( $this->db->escape(array
                (
                'nama'     => $this->input->post('nama'),
                'deskripsi'=> $this->input->post('deskripsi'),
                'pengurus' => $this->input->post('pengurus'),
                'socmed_1' => $this->input->post('socmed_1'),
                'socmed_2' => $this->input->post('socmed_2'),
                ))
            );

            if ($this->upload->do_upload('userfile'))
            {
                $upload_data = $this->upload->data();
                $image= $upload_data['file_name'];            
                $this->profil_model->update($unggah, array('picture'=>$image));            
            }

            $this->session->set_flashdata('response', show_msg('Input Profil berhasil disimpan!', 'success', '95%'));
            redirect(PANEL_URL.'/profil');
        }         

        $data['profil'] = '';        

        $this->template->title('Input Profil')
                ->build('admin/form',$data);         

    }

    public function delete($id){
        $this->profil_model->delete($id);
        $this->session->set_flashdata(show_msg('Profil berhasil dihapus', 'success'));
        redirect(PANEL_URL.'/profil');
    }

    public function edit($id){
        $id or redirect(''.PANEL_URL.'/users');
                
        $data['user'] = $this->profil_model->get($id);

        $data['id'] = $id;
        
        $this->form_validation->set_rules('nama', 'Nama Profil Komunitas', 'required|xss_clean');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|xss_clean');
        $this->form_validation->set_rules('pengurus', 'List Pengurus', 'required|xss_clean');

        if ($this->form_validation->run() == TRUE ){

            $this->upload_config();

            if ( ! $this->upload->do_upload('userfile') )
            {
                   // $data['error'] = show_msg($this->upload->display_errors(),'danger');
                 $this->profil_model->update($id, array(
                    'nama'     => $this->input->post('nama'),
                    'deskripsi'=> $this->input->post('deskripsi'),
                    'pengurus' => $this->input->post('pengurus'),
                    'socmed_1' => $this->input->post('socmed_1'),
                    'socmed_2' => $this->input->post('socmed_2'),
                   ));
                $this->session->set_flashdata('response',show_msg('Edit profil berhasil disimpan!', 'success', '95%'));
                redirect(PANEL_URL.'/profil');                 
            }
            else
            {
                unlink('./assets/upload/'.$data['user']->picture);

                $upload_data = $this->upload->data();
                $image = $upload_data['file_name'];
                
                $this->profil_model->update($id, array(
                    'nama'     => $this->input->post('nama'),
                    'deskripsi'=> $this->input->post('deskripsi'),
                    'pengurus' => $this->input->post('pengurus'),
                    'socmed_1' => $this->input->post('socmed_1'),
                    'socmed_2' => $this->input->post('socmed_2'),
                    'picture'  => $image,
                ));
                $this->session->set_flashdata('response',show_msg('Edit profil berhasil disimpan!', 'success', '95%'));
                redirect(PANEL_URL.'/profil'); 
            }
        } 
        
        $data['profil'] = $this->profil_model->get($id);

        $this->template->title('Edit Profil')
                ->build('admin/form',$data); 
        
        
    }
       
}


/* End of file admin.php */
/* Location: ./application/modules/news/controller/admin.php */
