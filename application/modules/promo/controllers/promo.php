<?php

class Promo extends Base_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('promo_model');

        $config['base_url'] = base_url('promo/hal/');
        $config['total_rows'] = 100;
        $config['per_page'] = 5; 
        $this->pagination->initialize($config); 
    }
    
    public function index()
    {
    	$data['tagline']	= 'Promo';
        $data['promo']		= $this->promo_model->get_all();

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();                
        $this->template->title('Portal Komunitas Jogja Gowes')
					   ->build('promo', $data);
    }

    public function hal($id = '')
    {
        $id = $this->uri->segment(3);

        $data['tagline']    = 'Promo Halaman '.$id;

        $this->promo_model->limit(5,$id);
        $data['promo']       = $this->promo_model->get_all();
        
        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();
        $this->template->title('Portal Komunitas Jogja Gowes')
                       ->build('promo', $data);
    }

    public function detail($id)
    {
        $data['id'] = $id;
        
        $data['tagline']    = 'Promo';
        $data['promo'] = $this->promo_model->get($id);
        
        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();
        $this->template->title('Portal Komunitas Jogja Gowes')
                       ->build('detail_promo', $data);       
    }
}

?>
