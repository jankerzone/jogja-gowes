<?php

class Admin extends Admin_Controller{
    
    
    public function __construct() {
        parent::__construct();
        $this->load->model('promo_model');
    }
    
    public function index(){
        $this->promo_model->order_by('id',$order = 'DESC');
        $data['promo']= $this->promo_model->get_all();

        $this->template->title('Promo Management')
                       ->append_metadata(theme_js('forms.js'))
                       ->build('admin/main',$data);
    }

    public function create(){
        $this->form_validation->set_rules('judul', 'Judul Promo', 'required|xss_clean');
        $this->form_validation->set_rules('konten', 'Konten Promo', 'required|xss_clean');
        $this->form_validation->set_rules('tanggal', 'Tanggal Publish', 'required|xss_clean');

        if($this->form_validation->run() == TRUE )
        {
            $this->promo_model->insert( $this->db->escape( array
                (
                'judul'     => $this->input->post('judul'),
                'konten'    => $this->input->post('konten'),
                'tanggal'   => $this->input->post('tanggal')
                ))
            );

            $this->session->set_flashdata('response', show_msg('Input promo berhasil disimpan!', 'success', '95%'));
            redirect(PANEL_URL.'/promo');
        }         

        $data['promo'] = '';        

        $this->template->title('Input Promo')
                ->build('admin/form',$data);         

    }

    public function delete($id){
        $this->promo_model->delete($id);
        $this->session->set_flashdata(show_msg('Promo berhasil dihapus', 'success'));
        redirect(PANEL_URL.'/promo');
    }

    public function edit($id){
        $id or redirect(''.PANEL_URL.'/users');
                
        $user = $this->promo_model->get($id);
                
        $data['id'] = $id;
        
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('', 'refresh');
        }

        //form validasi
        $this->form_validation->set_rules('judul', 'Judul Promo', 'required|xss_clean');
        $this->form_validation->set_rules('konten', 'Konten', 'required|xss_clean');
        $this->form_validation->set_rules('tanggal', 'Tanggal Publish', 'required|xss_clean');

        if($this->form_validation->run() == TRUE )
        {
            $this->promo_model->update($id,array(
                'judul'     => $this->input->post('judul'),
                'konten'    => $this->input->post('konten'),
                'tanggal'   => $this->input->post('tanggal')
                )
            );

            $this->session->set_flashdata('response', show_msg('Edit promo berhasil disimpan!', 'success', '95%'));
            redirect(PANEL_URL.'/promo');
        }                 

        $data['promo'] = $this->promo_model->get($id);

        $this->template->title('Edit Promo')
                ->build('admin/form',$data); 
        
        
    }
    

    
    
}


?>
