<h3>Dapatkan informasi seputar promo terkait Jogja-Gowes</h3>
<!-- content area -->    
	<section id="content">
    	<div class="clearfix">
	    	<?php foreach($promo as $row){ ?>
	    	<div class="grid_12">
	    	<h2><?php echo $row->judul ;?></h2>
	    	<p><?php echo word_limiter($row->konten, 25) ;?> <br/><?php echo anchor(BASE_URL.'promo/detail/'.$row->id.'/'. url_title($row->judul),'(baca selengkapnya)') ;?></p>
	    	</div>
	    	<?php } ?>
			<div class="grid_12">
	    		<center><?php echo $this->pagination->create_links();?></center>
	    	</div>
		</div>                    
	</section>
