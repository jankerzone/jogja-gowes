<?php echo anchor(PANEL_URL.'/promo/create','Tambah Promo','class="btn btn-primary"')?><br/><br/>
<table class="table table-striped table-bordered">
        <thead>
		<tr>
            <th>Judul Promo</th>
			<th>Konten</th>
			<th>Publish</th>
            <th>Aksi</th>
		</tr>
        </thead>
        <tbody>
            <?php foreach($promo as $row){?>
            <tr>
                <td width="100px"><?php echo $row->judul?></td>
                <td><?php echo nl2br(word_limiter($row->konten,50)) ?></td>
                <td width="100px"><?php echo $row->tanggal?></td>
                <td width="120px"><?php echo anchor(PANEL_URL.'/promo/edit/'.$row->id,'Edit','class="btn btn-warning"')?> 
                <?php echo anchor(PANEL_URL.'/promo/delete/'.$row->id,'Hapus','class="btn btn-danger"')?></td>
            </tr>
            <?php }?>
        </tbody>
        
</table>       