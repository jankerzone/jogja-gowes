<?php if (isset ($id)){echo form_open_multipart(''.PANEL_URL.'/promo/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.PANEL_URL.'/promo/create','class="form form-horizontal"');}?>
    <fieldset>
    
        <div class="control-group">
        <label class="control-label" for="judul">Judul Promo</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="judul" name="judul" value="<?php echo set_value('judul', $promo?$promo->judul:''); ?>">
             <?php echo form_error('judul'); ?>
        </div>     
        </div>
        
        <div class="control-group">
        <label class="control-label" for="konten">Konten Promo</label>
        <div class="controls"><textarea id="konten" style="height:200px; width:70%;" name="konten"><?php echo set_value('konten', $promo?$promo->konten:''); ?></textarea>
             <?php echo form_error('konten'); ?>
        </div>     
        </div>        
        
        <div class="control-group">
        <label class="control-label" for="tanggal">Tanggal Publish</label>
        <div class="controls">
            <input type="text" class="input-small" id="tanggal" name="tanggal" value="<?php if(isset($id)){ echo set_value('tanggal', $promo?$promo->tanggal:''); } else { $datestring = "%d-%m-%Y"; echo mdate($datestring); }; ?>">
             <?php echo form_error('tanggal'); ?>
        </div>     
        </div>      
        
        <div class="form-actions">
            <button class="btn btn-success">Simpan</button>
        </div>
        
    </fieldset>

<?php echo form_close()?>

