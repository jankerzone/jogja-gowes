<?php echo anchor(PANEL_URL.'/jalurgowes/create','Tambah Jalur Gowes','class="btn btn-primary"')?><br/><br/>
<table class="table table-striped table-bordered">
        <thead>
		<tr>
            <th>Nama Jalur</th>
			<th>Link</th>
			<th>Deskripsi</th>
            <th>Aksi</th>
		</tr>
        </thead>
        <tbody>
            <?php foreach($jalurgowes as $row){?>
            <tr>
                <td width="150px"><b><?php echo $row->jalur?></b></td>
                <td width="300px"><?php echo anchor($row->link, 'Klik disini') ?></td>
                <td><?php echo $row->deskripsi?></td>
                <td width="120px"><?php echo anchor(PANEL_URL.'/jalurgowes/edit/'.$row->id,'Edit','class="btn btn-warning"')?> 
                <?php echo anchor(PANEL_URL.'/jalurgowes/delete/'.$row->id,'Hapus','class="btn btn-danger"')?></td>
            </tr>
            <?php }?>
        </tbody>
        
</table>       