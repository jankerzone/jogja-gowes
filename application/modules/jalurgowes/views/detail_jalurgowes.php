<style>
    .google-maps {
        position: relative;
        padding-bottom: 75%; // This is the aspect ratio
        height: 0;
        overflow: hidden;
    }
    .google-maps iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }
</style>
<span class="clearfix"><?php echo anchor(base_url(),'Home') ;?> > <?php echo anchor('jalurgowes','Index Jalur Gowes') ;?> > <?php echo anchor(current_url(),$jalur->jalur) ;?></span>
<!-- content area -->    
	<section id="content">
    	<div class="clearfix">
	    	<div class="grid_12">
	    	<h1><?php echo $jalur->jalur ;?></h1>
	    	<div class="google-maps"><iframe src="<?php echo $jalur->link ?>" width="600" height="400" frameborder="0"></iframe></div>
	    	<span style="font-size:12px;"><?php echo anchor($jalur->link, 'Klik disini untuk tampilan penuh', 'target="blank"') ?></span>
	    	<hr/>
	    	<p><?php echo nl2br($jalur->deskripsi) ;?></p>
	    	</div>                    
	</section>
