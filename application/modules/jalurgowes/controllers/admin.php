<?php

class Admin extends Admin_Controller{
    
    
    public function __construct() {
        parent::__construct();
        $this->load->model('jalurgowes_model');
    }
    
    private function upload_config()
    {
      $config['upload_path'] = './assets/upload/';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['max_size']   = '2048'; //satuan kilobytes
      $config['max_width']  = '';
      $config['max_height']  = '';
      $config['encrypt_name'] = TRUE;
      $this->upload->initialize($config);
    }

    public function index()
    {
        $this->jalurgowes_model->order_by('id',$order = 'DESC');
        $data['jalurgowes']  = $this->jalurgowes_model->get_all();

        $this->template->title('Jalur Gowes Management')
                ->append_metadata(theme_js('forms.js'))
                ->build('admin/main',$data);
    }

    public function create()
    {
        $this->form_validation->set_rules('jalur', 'Jalur Gowes', 'required|xss_clean');
        $this->form_validation->set_rules('link', 'Link Jalur Gowes', 'required|xss_clean');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|xss_clean');

        if($this->form_validation->run() == TRUE )
        {
            $unggah = $this->jalurgowes_model->insert( $this->db->escape(array
                (
                'jalur'     => $this->input->post('jalur'),
                'link'      => $this->input->post('link'),
                'deskripsi' => $this->input->post('deskripsi')
                ))
            );

            $this->session->set_flashdata('response', show_msg('Input Jalur Gowes berhasil disimpan!', 'success', '95%'));
            redirect(PANEL_URL.'/jalurgowes');
        }         

        $data['jalur'] = '';        

        $this->template->title('Input Jalur Gowes')
                ->build('admin/form',$data);         

    }

    public function delete($id){
        $this->news_model->delete($id);
        $this->session->set_flashdata(show_msg('News berhasil dihapus', 'success'));
        redirect(PANEL_URL.'/news');
    }

    public function edit($id){
        $id or redirect(''.PANEL_URL.'/users');

        $data['id'] = $id;
        
        $this->form_validation->set_rules('jalur', 'Jalur Gowes', 'required|xss_clean');
        $this->form_validation->set_rules('link', 'Link Jalur Gowes', 'required|xss_clean');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|xss_clean');

        if ($this->form_validation->run() == TRUE ){           
                
                $this->jalurgowes_model->update($id, array(
                'jalur'     => $this->input->post('jalur'),
                'link'      => $this->input->post('link'),
                'deskripsi' => $this->input->post('deskripsi')
                ));
                $this->session->set_flashdata('response',show_msg('Edit jalur gowes berhasil disimpan!', 'success', '95%'));
                redirect(PANEL_URL.'/jalurgowes'); 
            }
         
        
        $data['jalur'] = $this->jalurgowes_model->get($id);

        $this->template->title('Edit Jalur Gowes')
                ->build('admin/form',$data); 
        
        
    }
       
}


/* End of file admin.php */
/* Location: ./application/modules/news/controller/admin.php */
