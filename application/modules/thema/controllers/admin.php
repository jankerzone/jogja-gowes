<?php

class Admin extends Admin_Controller{
    
    
    public function __construct() {
        parent::__construct();
        $this->load->model('thema_model');
    }
    
    public function index(){
        $data['themas']= $this->thema_model->get_all();
        $this->template->title('Thema Management')
                ->append_metadata(theme_js('forms.js'))
                ->build('admin/main',$data);
    }
    
    public function create(){
        
        $this->form_validation->set_rules('nama_thema', 'Nama Thema', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        
        if ($this->form_validation->run() == TRUE)
        {
                $this->thema_model->insert(array('nama_thema'=>$this->input->post('nama_thema'),
                        'harga'=>$this->input->post('harga'),
                        ));
                
                $this->session->set_flashdata(show_msg('Thema berhasil disimpan', 'success'));
                redirect(PANEL_URL.'/thema');
        }
        
        $data['thema'] = '';
        $this->template->title('Tambah thema')
                ->build('admin/form',$data);      
    }
    
    
}


?>
