<?php if (isset ($id)){echo form_open_multipart(''.PANEL_URL.'/thema/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.PANEL_URL.'/thema/create','class="form form-horizontal"');}?>
    <fieldset>
        
        <div class="control-group">
        <label class="control-label" for="nama_thema">Nama Thema</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="nama_gedung" name="nama_thema" value="<?php echo set_value('nama_thema', $thema?$thema->nama_thema:''); ?>">
             <?php echo form_error('nama_thema'); ?>
        </div>     
        </div>
        
        <div class="control-group">
        <label class="control-label" for="harga">Harga</label>
        <div class="controls">
            <input type="text" class="input-small" id="harga" name="harga" value="<?php echo set_value('harga', $thema?$thema->harga:''); ?>">
             <?php echo form_error('harga'); ?>
        </div>     
        </div>         
        
        <div class="form-actions">
            <button class="btn btn-success">Simpan</button>
        </div>
        
    </fieldset>

<?php echo form_close()?>