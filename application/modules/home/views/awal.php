
<!-- content area -->    
	<section id="content">
    	<div class="wrapper clearfix">
	    	
			<h1><?php echo $notifikasi->judul ?></h1>
			<h6><?php echo $notifikasi->publish ?></h6>
			<p><?php echo $notifikasi->konten ?></p>
			
		</div>
		<?php foreach($news as $row) :?>         
		<div class="grid_6">            
        <a href="<?php echo BASE_URL.'news/detail/'.$row->id.'/'. url_title($row->judul) ?>" ><img src="<?php echo BASE_URL."assets/upload/".$row->picture ?>" alt="jogja-gowes"/></a>
        <h3><?php echo $row->judul ?></h3>
        <h6><?php echo $row->tanggal ?></h6>
        <p><?php echo word_limiter($row->konten, 25) ;?> <br/><?php echo anchor(BASE_URL.'news/detail/'.$row->id.'/'. url_title($row->judul),'(baca selengkapnya)') ;?></p>
        </div>
        <?php endforeach ;?>
                    
	</section>
