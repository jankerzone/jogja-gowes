<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Base_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
        $this->load->model('home_model');
        $this->load->model('news_model');
        $this->load->model('event_model');
        
	}

	public function index()
	{
		$data['title']		= $this->testt();
		$data['tagline']	= 'Selamat datang <i>Goweser</i> !';
		$data['notif']		= 'notifikasi';
		$data['notifikasi'] = $this->home_model->get(1);

		$this->news_model->order_by('id',$order="DESC");
		$this->news_model->limit(6,0);
		$data['news']		= $this->news_model->get_all();

		$data['event_sidebar'] = $this->sidebar->ambil_event();
		$data['profil_sidebar']= $this->sidebar->ambil_profil();
		$this->template->title('Portal Komunitas Jogja Gowes')
					   ->build('awal', $data);
	}
        
    public function error404()
	{
		$data['title']		= "Duh, salah jalur gan, putar balik!";
		$this->load->view('error404',$data);
	}

	public function search()
	{
		$this->form_validation->set_rules('search', 'Kolom pencarian', 'required|xss_clean');
        
		$search = $this->input->post('search');

		if($this->form_validation->run() == TRUE) 
		{
			$this->db->like('judul',$search);
			$data['news']		= $this->news_model->get_all();
			
			$data['event_sidebar'] = $this->sidebar->ambil_event();
			$data['profil_sidebar'] = $this->sidebar->ambil_profil();
			$data['tagline']	= 'Selamat datang <i>Goweser</i> !';
			$this->template->title('Portal Komunitas Jogja Gowes')
					   ->build('search', $data);
		} 
		else 
		{
			$this->form_validation->set_message('required', 'Kolom pencarian tidak boleh kosong');
			redirect('home');
		}
	}
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */