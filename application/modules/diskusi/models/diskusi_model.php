<?php


class Diskusi_model extends MY_Model{
    public function __construct() {
        parent::__construct();

    //protected $_table = 'diskusi';
    }
   
    public function get_user()
    {
        $this->db->select('diskusi.*, kategori.nama, users.username');
    	$this->db->from('diskusi');
    	$this->db->join('users', 'users.id = diskusi.id_user');
        $this->db->join('kategori', 'kategori.id = diskusi.id_kategori');
        $this->db->where('id_komen', '0');
    	//$this->db->order_by('waktu','ASC');
    	return $this->db->get()->result();
    }

    //ambil kategori tanpa fjb
    public function get_kat()
    {
        $this->db->select('*');
        $this->db->not_like('id','5');
        $this->db->from('kategori');

        return $this->db->get()->result();
    }

    public function get_trit($id)
    {
    	$this->db->select('diskusi.*, users.username');
    	$this->db->from('diskusi');
    	$this->db->join('users', 'users.id = diskusi.id_user');
    	//$query = $this->db->get();
    	return $this->db->get()->result();
    } 

    public function get_reply($id)
    {
        $this->db->select('diskusi.*, users.username');
        $this->db->from('diskusi');
        $this->db->join('users', 'users.id = diskusi.id_user');
        $this->db->where('diskusi.id', $id);
        $query = $this->db->get()->row();
        return $query;


    }    


    public function get_komen($id)
    {
        $this->db->select('diskusi.*, users.username');
        $this->db->from('diskusi');
        $this->db->join('users', 'users.id = diskusi.id_user');
        $this->db->where('id_komen', $id);
        $this->db->order_by('urutan', 'ASC');
        return $this->db->get()->result();
    }    

    public function get_single($id)
    {
    	$this->db->select('diskusi.*, users.username');
    	$this->db->from('diskusi');
    	$this->db->join('users', 'users.id = diskusi.id_user');
    	//$query = $this->db->get();
    	return $this->db->get()->result();
    }

    
}

?>
