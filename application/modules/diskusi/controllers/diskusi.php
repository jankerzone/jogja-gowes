<?php

class Diskusi extends Base_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('diskusi_model');

        $config['base_url'] = base_url('diskusi/hal/');
        $config['total_rows'] = 100;
        $config['per_page'] = 5; 
        $this->pagination->initialize($config); 

    }
    
    public function index()
    {
        $data['event_sidebar'] = $this->sidebar->ambil_event();
    	$data['tagline']	= 'Papan Diskusi';

        $data['board']		= $this->diskusi_model->get_user();

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('Papan Diskusi :: Portal Komunitas Jogja Gowes')
					   ->build('diskusi-index', $data);
    }

    public function trit()
    {
        $id = $this->uri->segment(3);
        
        if( ! $id){
            redirect('diskusi','refresh');
        }

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['tagline']       = 'Papan Diskusi';

        $this->db->where('diskusi.id',$id);
        $data['board_ts']         = $this->diskusi_model->get_trit($id);
        
        $data['board_komen']      = $this->diskusi_model->get_komen($id);
        
        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('Papan Diskusi :: Portal Komunitas Jogja Gowes')
                       ->build('diskusi-trit', $data);

    }

    public function post($id)
    {
        $id = $this->uri->segment(3);
        
        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['tagline']       = 'Papan Diskusi';

        $this->db->where('diskusi.id',$id);
        $data['board_ts']         = $this->diskusi_model->get_trit($id);
        
        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('Single Post :: Portal Komunitas Jogja Gowes')
                       ->build('diskusi-post', $data);

    }

    public function hal($id = '')
    {
        $id = $this->uri->segment(3);

        $data['tagline']    = 'News Halaman '.$id;

        $this->news_model->limit(5,$id);
        $data['news']       = $this->news_model->get_all();
        
        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('News :: Portal Komunitas Jogja Gowes')
                       ->build('news', $data);
    }



    public function create()
    {
        $data['tagline']    = 'Bikin Trit';

        $data['id_trit']    = random_string('alnum',10);
        $data['id_user']    = $this->ion_auth->user()->row();
        $data['kat']        = $this->diskusi_model->get_kat();
        $data['diskusi']    = '';

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('Bikin Trit :: Portal Komunitas Jogja Gowes')
                       ->build('diskusi-form', $data);          
    }

    function create_proses()
    {
        
        $this->form_validation->set_rules('judul', 'Judul trit', 'required|xss_clean');
        $this->form_validation->set_rules('konten', 'Konten', 'required|xss_clean');
        $this->form_validation->set_rules('kategori', 'Kategori', 'required|xss_clean');
        
        if($this->form_validation->run() == TRUE )
        {
            $unggah = $this->db->insert( 'diskusi', $this->db->escape(array
                    (
                        'id'        => $this->input->post('id'),
                        'id_user'   => $this->input->post('id_user'),
                        'judul'     => $this->input->post('judul'),
                        'waktu'     => $this->input->post('waktu'),
                        'konten'    => $this->input->post('konten'),
                        'edited'    => $this->input->post('edited'),
                        'id_komen'  => $this->input->post('id_komen'),
                        'id_kategori'=> $this->input->post('kategori'),
                        'urutan'    => $this->input->post('urutan'),
                    )
                ));
            echo 'Anda akan teralih ke halaman lain sebentar lagi...';
            $this->session->set_flashdata('response', show_msg('Trit berhasil dibuat!', 'success', '95%'));
            redirect(SITE_URL().'diskusi/trit/'.$this->input->post('id'));
        }
    }

    function reply($id)
    {
        $id = $this->uri->segment(3);

        $data['tagline']    = 'Komentar';
        $data['id_id']   = random_string('alnum',10);
        $data['id_user']    = $this->ion_auth->user()->row();
        
        $data['diskusi']    = $this->diskusi_model->get_reply($id);

        $data['event_sidebar'] = $this->sidebar->ambil_event();
        $data['profil_sidebar']= $this->sidebar->ambil_profil();        
        $this->template->title('Balas Komentar :: Portal Komunitas Jogja Gowes')
                       ->build('diskusi-reply', $data);          
    }

    public function reply_proses()
    {
        
        //$this->form_validation->set_rules('judul', 'Judul trit', 'required|xss_clean');
        $this->form_validation->set_rules('konten', 'Konten', 'required|xss_clean');
        //$this->form_validation->set_rules('kategori', 'Kategori', 'required|xss_clean');
        
        if($this->form_validation->run() == TRUE )
        {
            $unggah = $this->db->insert( 'diskusi', $this->db->escape(array
                    (
                        'id'        => $this->input->post('id'),
                        'id_user'   => $this->input->post('id_user'),
                        'judul'     => $this->input->post('judul'),
                        'waktu'     => $this->input->post('waktu'),
                        'konten'    => $this->input->post('konten'),
                        'edited'    => $this->input->post('edited'),
                        'id_komen'  => $this->input->post('id_komen'),
                        'id_kategori'=> $this->input->post('kategori'),
                        'urutan'    => $this->input->post('urutan'),
                    )
                ));
            echo 'Anda akan teralih ke halaman lain sebentar lagi...';
            $this->session->set_flashdata('response', show_msg('Trit berhasil dibuat!', 'success', '95%'));
            redirect(SITE_URL().'diskusi/trit/'.$this->input->post('id_komen'));
        }
    }

}

?>
