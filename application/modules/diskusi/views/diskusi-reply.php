
<?php   
$datestring = "%d-%m-%Y %h:%i";

$hidden = array(
    'id'        => random_string('alnum',10),
    'id_user'   => $id_user->id,
    'waktu'     => mdate($datestring),
    'edited'    => '0',
    'id_komen'  => $diskusi->id,
    'urutan'    => $diskusi->urutan + 1,
    'kategori'  => $diskusi->id_kategori,
    );
?>
<?php echo $this->load->view('atas');?>
<?php if (isset ($id)){echo form_open_multipart(SITE_URL().'/diskusi/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.SITE_URL().'diskusi/reply_proses','class="form form-horizontal"',$hidden);}?>
    <section id='content'>
        <div>In reply to <?php echo $diskusi->judul ?></div>  
        <div class="control-group">
        <label class="control-label" for="judul">Judul</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="judul" name="judul">
             <?php echo form_error('judul'); ?>
        </div>     
        </div>
        
        <div class="control-group">
        <label class="control-label" for="konten"></label>
        <div class="controls"><textarea id="konten" style="height:200px; width:70%;" name="konten"></textarea>
             <?php echo form_error('konten'); ?>
        </div>     
        </div>        
            
        <div class="form-actions">
            <button class="btn btn-success" value="submit">Submit</button>
        </div>
        
<?php echo form_close()?>
</section>
