<span class="clearfix"><?php echo anchor(base_url(),'Home') ;?> > <?php echo anchor('news','Index News') ;?> > <?php echo anchor(current_url(),$news->judul) ;?></span>
<!-- content area -->    
	<section id="content">
    	<div class="clearfix">

	    	<div class="grid_12">
	    	<h1><?php echo $news->judul ;?></h1>
	    	<span><?php echo $news->tanggal ;?></span><br/>
	    	<?php if($news->picture != NULL)
                {
                    echo '<img src="'.base_url('assets/upload/'.$news->picture).'" title="'.$news->judul.'" width="350px" style="padding-right:10px; float:left" />'; 
                } else {
                    echo '';
                }?>
	    	<p><?php echo nl2br($news->konten) ;?></p>
	    	</div>
	    	<div class="grid_12">
	    		<hr/>
	    		<span>Berita lainnya</span>
	    		<ul>
	    		<?php foreach($terkait as $row): ?>
	    			<li><?php echo anchor(BASE_URL.'news/detail/'.$row->id.'/'. url_title($row->judul),$row->judul) ;?></li>
	    		<?php endforeach ;?>
	    		</ul>
		</div>                    
	</section>
