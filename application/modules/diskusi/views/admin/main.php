<?php echo anchor(PANEL_URL.'/news/create','Tambah News','class="btn btn-primary"')?><br/><br/>
<table class="table table-striped table-bordered">
        <thead>
		<tr>
            <th>Judul Berita</th>
			<th>Konten</th>
			<th>Publish</th>
            <th>Aksi</th>
		</tr>
        </thead>
        <tbody>
            <?php foreach($newss as $news){?>
            <tr>
                <td width="100px"><b><?php echo $news->judul?></b></td>
                <td><?php if($news->picture != NULL)
                {
                    echo '<img src="'.base_url('assets/upload/'.$news->picture).'" width="250px" class="pull-left" style="padding-right:5px;"/>'; 
                } else {
                    echo '<img src="'.base_url('assets/upload/noimg.gif').'" width="250px" class="pull-left" style="padding-right:5px;"/>';
                }?>
                <p><?php echo nl2br(word_limiter($news->konten,50))?></p></td>
                <td width="100px"><?php echo $news->tanggal?></td>
                <td width="120px"><?php echo anchor(PANEL_URL.'/news/edit/'.$news->id,'Edit','class="btn btn-warning"')?> 
                <?php echo anchor(PANEL_URL.'/news/delete/'.$news->id,'Hapus','class="btn btn-danger"')?></td>
            </tr>
            <?php }?>
        </tbody>
        
</table>       