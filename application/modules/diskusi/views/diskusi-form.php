<?php 
$datestring = "%d-%m-%Y %h:%i";
$hidden = array(
    'id'        => $id_trit,
    'id_user'   => $id_user->id,
    'waktu'     => mdate($datestring),
    'edited'    => '0',
    'id_komen'  => '0',
    'urutan'    => '1'
    );
?>
<?php echo $this->load->view('atas');?>
<?php if (isset ($id)){echo form_open_multipart(SITE_URL().'/diskusi/edit/'.$id,'class="form form-horizontal"');}else{ echo form_open_multipart(''.SITE_URL().'diskusi/create_proses','class="form form-horizontal"',$hidden);}?>
    <section id='content'>
    
        <div class="control-group">
        <label class="control-label" for="judul">Judul Trit</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="judul" name="judul" value="<?php echo set_value('judul', $diskusi?$diskusi->judul:''); ?>">
             <?php echo form_error('judul'); ?>
        </div>     
        </div>
        
        <div class="control-group">
        <label class="control-label" for="konten">Konten</label>
        <div class="controls"><textarea id="konten" style="height:200px; width:70%;" name="konten"><?php echo set_value('konten', $diskusi?$diskusi->konten:''); ?></textarea>
             <?php echo form_error('konten'); ?>
        </div>     
        </div>        

        <div class="control-group">
        <label class="control-label" for="kategori">Kategori</label>
        <select name="kategori">
            <option>----</option>
            <?php foreach($kat as $row):?>
            <option value="<?php echo $row->id ?>"><?php echo $row->nama ?></option>    
        <?php endforeach ?>
        </select>
             <?php echo form_error('kategori'); ?>
     
        </div>         
              
        <div class="form-actions">
            <button class="btn btn-success" value="submit">Submit</button>
        </div>
        
<?php echo form_close()?>
</section>
