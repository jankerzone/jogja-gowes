<?php echo $this->load->view('atas');?>
<!-- content area -->    
	<section id="content">
    	<div class="clearfix">
    		<div class="grid_12">
		    	<?php foreach($board as $row): ?>
		    	<h3><?php echo anchor(SITE_URL().'diskusi/trit/'. $row->id . '/' . url_title($row->judul), $row->judul ) ?></h3>
		    	<span>Dipost oleh <a href="<?php echo SITE_URL() . 'users/id/' . $row->id_user ?>"><?php echo $row->username ?></a></span>
		    	<span>Pada <?php echo $row->waktu ?></span> - 
		    	<span><?php echo anchor(SITE_URL().'diskusi/kategori/' . $row->id_kategori , $row->nama) ?></span>
		    	<?php endforeach ?>
		    </div>
	    	<div class="grid_12">
	    		<center><?php echo $this->pagination->create_links();?></center>
	    	</div>
		</div>                    
	</section>
