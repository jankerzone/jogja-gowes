<?php

/**
 * @author 		NPHS Team
 */

class Admin extends Admin_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('users/user_model');
    }
    


    public function index()
    {
  			
  			if($this->ion_auth->in_group(array('root')))
        {
            $this->template->title('Welcome Administrator')
            ->build('dashboard');
        }
    }
    
}

?>
