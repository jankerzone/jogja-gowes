---- Tentang Jogja Gowes -----

Aplikasi Jogja-Gowes dibuat dari beberapa source dan tools sbb :
	- Simple-responsive-template by Prowebdesign.ro (HTML Responsive Themes with jQuery)
	- Bootstrap v3.2.0 by Twitter Bootstrap
	- PHP Framework Codeigniter 2.1.4
	- Codeigniter Template Master by Philsturgeon
	- CodeIgniter Ion-Auth 2.5.2 by Benedmunds (Login Auth)
	- Codeigniter Modular Extensions HMVC by Wiredesignz
	- Apache Web Server 2.4.3
	- Google Font API
	- jQuery UI 1.9.0
	- Modernizr Javasript Framework 2.6.2
	- PHP 5.4.7
	- Yepnope.js 
	- Sublime Text Editor Build 3047

	
Source code repository : git@bitbucket.org:jankerzone/jogja-gowes.git

Dibuat dengan ditemani kopi pada 2014.